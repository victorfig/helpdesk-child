<?php get_header(); ?>

<?php global $newsCatObj; $newsCatObj = $wp_query->queried_object;?>

<div id="content" class="clearfix row-fluid">

	<div id="main" class="span8 clearfix" role="main">

		<!-- Breadcrumb -->
		<small style="font-size:12px;">
			
			<ul class="breadcrumb">
		    
		        <li><a href="/">HOME</a> <span class="divider">/</span></li>
		    
		        <li class="active"><a href="?cat=52">NEWS</a> <span class="divider">/</span></li>
		    
		        <li class="active"><?php echo $assignment_name = strtoupper($newsCatObj->name); ?></li>
	    	
	    	</ul>
	    
	    </small>

	    <?php printSanitizedPageTitle($newsCatObj->slug); ?>

		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					
			<div class="featurette">

				<?php the_post_thumbnail( 'thumbnail', array('class' => 'featurette-image pull-left thumbnail-right-padding' ) ); ?>
			
				<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">
						
					<header id="<?php echo $tag_list = commaSeparatedTagList($q->post->ID, '' ,'category', ' '); ?>">

						<h4 class="article-title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h4>
						
						<small class="small muted"><?php _e("Posted", "bonestheme"); ?> <time datetime="<?php echo the_time('Y-m-j'); ?>" pubdate><?php the_date(); ?></time></small>
					
					</header> <!-- end article header -->
					
					<section class="post_content">

						<?php the_excerpt(); ?>
					
					</section> <!-- end article section -->
				
				</article> <!-- end article -->

			</div>

			<?php $tags[] = get_the_terms($post->ID, 'category'); ?>
			
			<?php endwhile; ?>	
			
			<?php if (function_exists('jcah_page_navi')) { // if expirimental feature is active ?>
						
				<?php jcah_page_navi( ); // use the page navi function ?>
				
			<?php } else { // if it is disabled, display regular wp prev & next links ?>
				<nav class="wp-prev-next">
					<ul class="clearfix">
						<li class="prev-link"><?php next_posts_link(_e('&laquo; Older Entries', "bonestheme")) ?></li>
						<li class="next-link"><?php previous_posts_link(_e('Newer Entries &raquo;', "bonestheme")) ?></li>
					</ul>
				</nav>
			<?php } ?>
						
			
			<?php else : ?>
			
			<article id="post-not-found">
			    <header>
			    	<h1><?php _e("No Posts Yet", "bonestheme"); ?></h1>
			    </header>
			    <section class="post_content">
			    	<p><?php _e("Sorry, What you were looking for is not here.", "bonestheme"); ?></p>
			    </section>
			    <footer>
			    </footer>
			</article>
			
		<?php endif; ?>
		
	</div> <!-- end #main -->

	<?php get_sidebar(); // sidebar 1 ?>

</div> <!-- end #content -->

<?php get_footer(); ?>
