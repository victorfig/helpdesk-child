<?php get_header(); $currentPage = 'taxonomy_doc_tag_faq'; $assignmentSessionVar = $_SESSION['assignment']; ?>

<?php check_current_user_assignment_permissions($assignmentSessionVar); ?>

<?php $catObj = $wp_query->queried_object; $faqTag = 'frequently-asked-questions'; $trainingTag = 'training'; ?>

<?php $tagToOmit = get_term_by('name','training','doc_tag'); // Do not include training post in query ?>

<?php $args = array('post_type' => array('document','jcah_faq'), 'doc_tag' => $faqTag, 'assignments' => $assignmentSessionVar, 'posts_per_page' => '-1', 'tag__not_in' => array($tagToOmit->term_id)); ?>

<?php $faq = new WP_Query( $args ); ?>

<div id="content" class="clearfix row-fluid">

	<div id="main" class="span8 clearfix" role="main">

		<!-- Breadcrumb -->
		<small style="font-size:12px;">
			
			<ul class="breadcrumb">
		    
		        <li><a href="/">HOME</a> <span class="divider">/</span></li>
		    
		        <li><a href="?assignments=<?php echo $assignmentSessionVar; ?>"><?php echo $assignment_name = strSantizeTagBreanCrumb($assignmentSessionVar);?></a> <span class="divider">/</span></li>
		    
		        <?php if(empty($secondTag)):?>
		    
		        	<li class="active"><?php echo $doc_tag_name = strtoupper($catObj->name); ?></li>
		    
		        <?php endif;?>

		        <?php if(!empty($secondTag)):?>
		    
		        	<li class="active"><?php echo $doc_tag_name = strtoupper($catObj->name); ?></a> <span class="divider">/</span></li>
		    
		        	<li class="active"><?php echo $second_doc_tag_name = strtoupper($secondTag); ?></li>
		    
		        <?php endif;?>
	    	
	    	</ul>
	    
	    </small>

	    <?php if(!empty($faq->posts)): // Posts are required ?>

	    	<!-- Prepare to create pdf -->
	    	<?php $pdf = new JCAHPDF(); ?>

	    	<?php $pdf->AssignmentName($assignment_name . ' FAQs'); ?>

	    	<?php $pdf->AddPage(); ?>
	    
		    <?php printSanitizedPageTitle($assignmentSessionVar); ?>
			
			<?php if ( $faq->have_posts() ) : while ( $faq->have_posts() ) : $faq->the_post();?>

					<?php $tags[] = get_the_terms($faq->post->ID, 'doc_tag'); ?>
			
			<?php endwhile; else: ?> <!-- end loop -->
			
				<span class="label label-inverse"><?php _e('No tags available'); ?></span>
			
			<?php endif; ?>
			
			<?php rewind_posts(); ?>

			<?php $postTags = createSimpleArrayFromMultiDimensionalArray($tags); ?>

			<?php if(!empty($postTags)):?>

					<a href="#_" class="expandcollapse btn btn-mini btn-primary pull-right" state="0"><i class='icon-white icon-minus-sign'></i> Collapse All</a> <?php global $collapseBtn; $collapseBtn = true; ?>

					<a href="/wp-content/uploads/generated-pdfs/<?php echo $assignment_name;?>-faqs.pdf" target="_blank" style="margin-right:5px;" class="btn btn-mini btn-danger pull-right"><i class='icon-white icon-download-alt'></i> PDF</a>
				
				<div class="clearfix">&nbsp;</div>

				<div >

					<?php foreach($postTags as $postTag) { $thisPostTags[]=$postTag['slug']; }?>

						<?php foreach($thisPostTags as $thisPostTag):?>

							<a name="<?php echo $thisPostTag; ?>"></a>

							<?php if($thisPostTag != $faqTag && $thisPostTag != $trainingTag):?>
								
								<div class="accordion">
								    <!-- Sections -->

								    <div class="accordion-group faq-remove-borders">
								        <!-- Section -->
								            
							            <script type="text/javascript">
										
											jQuery(document).ready(function($) {
											    
											    $('.filterinput-<?php echo $thisPostTag;?>').keyup(function() {
											        
											        var input = $(this).val();
											        
											        if (input.length > 2) {
											            
											            // this finds all items in the list that contain the input,
											            // and hide the ones not containing the input while showing the ones that do
											            var containing = $('#list-<?php echo $thisPostTag;?> .accordion-heading').filter(function () {
											        
											                var regex = new RegExp('\\b' + input, 'gi');
											                
											                return regex.test($(this).text());

											            }).slideDown();
											        
											            $('#list-<?php echo $thisPostTag;?> .accordion-heading').not(containing).slideUp();
											        
											        } else {
											        
											            $('#list-<?php echo $thisPostTag;?> .accordion-heading').slideDown();
											        
											        }
											        
											        return false;
											    
											    })

											});

										</script>

										<div class="accordion-heading">

								            <h4>
								            
								            	<a href="#top"><i class="icon-arrow-up faq-up-arrow pull-left" style="margin-top:2px;"></i></a>

								            	<a class="accordion-toggle faq-section" data-toggle="collapse" href="#collapse-<?php echo $thisPostTag;?>">
								        
													<?php echo $faqSection = strSantizeTagStrings($thisPostTag); ?>

													<?php $pdf->SetTextColor(44, 62, 80); $pdf->SetFont('Helvetica','B',16); $pdf->Cell(40,10,$faqSection); $pdf->Ln();?>
								        
								            	</a>

								            </h4>
								        
								        </div><!-- /Section -->

										<div class="accordion-body collapse in" id="collapse-<?php echo $thisPostTag;?>">

											<div style="height:9px;">&nbsp;</div>

											<ul id="list-<?php echo $thisPostTag;?>">

												<div style="width:100%;">

													<div class="input-prepend">

														<span class="add-on" style="padding:5px 5px;background-color:#d7e0e2;"><li class="icon icon-search"></li></span>
														
														<input class="filterinput-<?php echo $thisPostTag;?> input-large" type="text" placeholder="Start typing..." style="padding:3px 5px;border:2px solid #d7e0e2;">
													
													</div>
												
												</div>

									            <div class="accordion-inner faq-remove-borders">
									                
									                <div class="accordion" id="<?php echo $thisPostTag;?>">
									                    <!-- SubSections -->

														<?php if ( $faq->have_posts() ) : while ( $faq->have_posts() ) : $faq->the_post();?>

															<?php if(is_object_in_term($faq->post->ID, 'doc_tag', $thisPostTag)):?>

											                    <div class="accordion-group faq-remove-borders">
											                    
											                        <div class="accordion-heading question">
											                    
											                            <li class="accordion-toggle" data-parent=
											                            "#<?php echo $thisPostTag;?>-<?php the_ID(); ?>" data-toggle="collapse" href=
											                            "#showQuestion<?php echo $thisPostTag;?>-<?php the_ID(); ?>" style="text-decoration:none;"><span style="color:#1abc9c;"><?php the_title(); $title = iconv('UTF-8', 'windows-1252', get_the_title()); ?></span>

																			<?php $pdf->Cell(5); $pdf->SetFont('Helvetica','',14); $pdf->MultiCell(0,5,str_replace('&#8217;',"'",$title)); $pdf->Ln();?>
																		
																		</li>
												                        
												                        <div class="accordion-body collapse in" id="showQuestion<?php echo $thisPostTag;?>-<?php the_ID(); ?>">
												                    
												                            <div class="accordion-inner faq-remove-borders">
												                    
												                                <div class="accordion" id="showQuestion<?php echo $thisPostTag;?>-<?php the_ID(); ?>">
												                    
												                                    <div class="accordion-group faq-remove-borders">
												                                        
												                                        <div style="margin-top:-12px;">
													                                    
													                                        <?php if($faq->post->post_type == 'jcah_faq'): ?>
																						
																								<small class="muted">Updated <?php echo $date = formatModifiedDate($faq->post->post_modified); ?></small>

																								<?php $pdf->Cell(10); $pdf->SetFont('Helvetica','I',9); $pdf->MultiCell(0,2,'Modified ' . $date); $pdf->Ln();?>
																								
																								<div class="clearfix"><?php the_content(); ?></div>

																								<?php $strToSearch = get_the_content(); $strJwplayer = 'jwplayer'; $strVideo = 'video'; ?>						

																								<?php if(false !== stripos($strToSearch,$strVideo) || false !== stripos($strToSearch,$strJwplayer)): ?>

																									<?php $pdf->Cell(10); $pdf->SetFont('Helvetica','',11); $pdf->Cell(10,10,'Because a video is included in this FAQ you must view it on the Helpdesk.'); $pdf->Ln(10);?> 

																								<?php else:?>
																								
																									<?php $pdf->Cell(10); $pdf->SetFont('Helvetica','',11); $pdf->MultiCell(0,5,iconv('UTF-8', 'windows-1252', get_the_content()),0,'L',false); $pdf->Ln(10);?>

																								<?php endif;?>
																								
																								<!-- Retreive associated documents | Also commented in jcahCustomFields.php under Custom-code folder -->
																								<?php /* if(!empty($faq->post) && count($faq->posts) > 1):?>
																									
																									<?php $relatedDocuments = get_fields($faq->post->ID);?>

																									<?php foreach($relatedDocuments as $relatedDocument):?>

																										<?php if(!empty($relatedDocument)):?>

																											<div class="clearfix">

																												<?php foreach($relatedDocument as $relDocument):?>
																													
																													<div style="margin-bottom:5px;">
																													
																														<strong><?php echo $relDocument->post_title;?></strong><br />
																													
																														<i class="icon-file"></i> <?php echo '<a href="/?post_type=document&p='. $relDocument->ID .'">Download</a> | <small class="muted">Modified ' . $date = formatModifiedDate($relDocument->post_modified) . '</small>'; ?>
																													
																													</div>
																												
																												<?php endforeach;?>

																											</div>

																										<?php endif; // End checking if relatedDocuments array is empty ?>
																									
																									<?php endforeach;?>
																								
																								<?php endif; */ ?>

																							<?php elseif($faq->post->post_type == 'document'):?>
																						
																								<small class="muted">Updated <?php echo $date = formatModifiedDate($faq->post->post_modified); ?></small>

																								<?php $pdf->Cell(10); $pdf->SetFont('Helvetica','I',9); $pdf->MultiCell(0,2,'Modified ' . $date . ' (Document)'); $pdf->Ln();?> | <a href="<?php the_permalink();?>"><i class="icon-download-alt"></i> Download</a>
																								
																								<div class="clearfix"><?php $desc = get_field("description"); echo (!empty($desc) ? '<div>' . $desc . '</div>' : '') ?></div>

																								<?php $pdf->Cell(10); $pdf->SetFont('Helvetica','',11); $pdf->MultiCell(0,5,iconv('UTF-8', 'windows-1252', $desc),0,'L',false); $pdf->Ln(10);?>

																							<?php endif;?>

																						</div>

												                                    </div>
												             
												                                </div>
												             
												                            </div>
												             
												                        </div><!-- /Questions -->

												                    </div><!-- Questions -->
											             
											                    </div><!-- /SubSection -->
													
															<?php endif;?> <!-- End of Term Check -->
													
														<?php endwhile; else: ?> <!-- end while loop -->
													
															<span class="label label-inverse"><?php _e('No Documents associated with this tag'); ?></span>
													
														<?php endif; // end loop if statement?>

													</div><!-- /SubSections -->

									            </div>

								            </ul>

								        </div>
									
									</div> <!-- End accordion-group div -->
								
								</div> <!-- End accordion div -->

							<?php endif;?> <!-- End if thisPostTag != faq -->

						<?php endforeach; ?> <!-- End foreach thisPostTag -->

					</div>

			<?php endif; ?>
			
			<?php wp_reset_postdata(); //Restore original Post Data ?>

			<?php $pdf->Output(get_jcah_wp_content('basedir') . '/generated-pdfs/' . $assignment_name . '-faqs.pdf','F'); ?>

		<?php else: ?>

			<?php if(current_user_can('edit_post')): ?>
				
				<div class="alert alert-block">
					
					<h4 class="alert-heading">Alert</h4>
					
					<p>You must be looking for an Frequently Asked Question that you added recently. If you are, and it's not showing, it means you forgot to add the 'Frequently Asked Questions' tag to the post, please double check it.</p>
					
				</div>

			<?php endif;?>

		<?php endif; ?>
	
	</div> <!-- end #main -->

	<?php get_sidebar(); // sidebar 1 ?>

</div> <!-- end #content -->

<?php if(isset($collapseBtn)):?>
	<script type="text/javascript">
		jQuery(document).ready(function($) {

			$('.filterinput').keyup(function() {
			        var a = $(this).val();
			        if (a.length > 2) {
			            // this finds all links in the list that contain the input,
			            // and hide the ones not containing the input while showing the ones that do
			            var containing = $('#list li').filter(function () {
			                var regex = new RegExp('\\b' + a, 'i');
			                return regex.test($('a', this).text());
			            }).slideDown();
			            $('#list li').not(containing).slideUp();
			        } else {
			            $('#list li').slideDown();
			        }
			        return false;
			    })

			//collapsible menu
		    $('.expandcollapse').click(function() {

		        var newstate = $(this).attr('state') ^ 1,
		            icon = newstate ? "plus" : "minus",
		            text = newstate ? "Expand" : "Collapse";
		    
		        // if state=0, show all the accordion divs within the same block (in this case, within the same section)
		        if ( $(this).attr('state')==="0" ) {
		            console.log('2');
		            $(this).siblings('div').find('div.accordion-body.in').collapse('hide');
		        }
		        // otherwise, collapse all the divs
		        else {
		            console.log('1');
		            $(this).siblings('div').find('div.accordion-body:not(.in)').collapse('show');
		        }

		        $(this).html("<i class=\"icon-white icon-" + icon + "-sign\"></i> " + text +" All");

		        $(this).attr('state',newstate)

		    });

		    $('a[data-toggle="tab"]').on('shown', function (e) {

		        var myState = $(this).attr('state'),
		            state = $('.expandcollapse').attr('state');

		        if(myState != state) {
		          toggleTab($(this).prop('hash'));
		          $(this).attr('state',state);
		        }

		    })

		    function toggleTab(id){

		        $(id).find('.collapse').each(function() {
		            $(this).collapse('toggle');
		          });

		    }
	    });
	</script>
<?php endif; ?>

<?php get_footer(); ?>
