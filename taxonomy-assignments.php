<?php get_header(); $currentPage = 'taxonomy_assignment';?>

<?php $catObj = $wp_query->queried_object; $assignmentSlug = $catObj->slug;?>

<?php check_current_user_assignment_permissions($assignmentSlug); ?>

<?php $args = array('post_type' => 'document', 'assignments' => $catObj->slug, 'paged' => $paged); $_SESSION['assignment'] = $catObj->slug; ?>

<?php $doc = new WP_Query( $args ); ?>

<div id="content" class="clearfix row-fluid">

	<div id="main" class="span8 clearfix" role="main">

		<!-- Breadcrumb -->
		<small style="font-size:12px;">
			<ul class="breadcrumb">
		        <li><a href="/">HOME</a> <span class="divider">/</span></li>
		        <li class="active"><?php echo $assignment_name = strtoupper($catObj->name); ?></li>
	    	</ul>
	    </small>

	    <?php printSanitizedPageTitle($catObj->name); ?>

		<?php if ( $doc->have_posts() ) : while ( $doc->have_posts() ) : $doc->the_post();?>

			<article id="post-<?php the_ID(); ?>" title="<?php the_title(); ?>" <?php post_class('clearfix'); ?> role="article" style="margin-bottom:0px !important">
			
				<header id="<?php echo $tag_list = commaSeparatedTagList($doc->post->ID, '' ,'doc_tag', ' '); ?>" class="doc-description">

					<h4><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h4>
				
				</header> <!-- end article header -->
				
				<section class="post_content">
					
					<?php $desc = get_field("description"); echo (!empty($desc) ? '<div>' . $desc . '</div>' : '') ?>
					
					<small class="muted">Updated On: <?php echo $date = formatModifiedDate($doc->post->post_modified); ?></small>

					<?php list_jcah_document_revisions($doc->post->ID); ?>
				
				</section> <!-- end article section -->
				
				<!-- <footer>
				
				</footer> --> <!-- end article footer -->
			
			</article> <!-- end article -->

			<?php $tags[] = get_the_terms($doc->post->ID, 'doc_tag'); ?>
	
		<?php endwhile; else: ?> <!-- end loop -->
				
			<span class="label label-inverse clearfix"><?php _e('No Documents have been added to this assignment'); ?></span>
		
		<?php endif; ?>

		<?php if (function_exists('jcah_page_navi')) { // if expirimental feature is active ?>
						
			<?php jcah_page_navi('','',$doc ); // use the page navi function ?>
			
		<?php } else { // if it is disabled, display regular wp prev & next links ?>
			<nav class="wp-prev-next">
				<ul class="clearfix">
					<li class="prev-link"><?php next_posts_link(_e('&laquo; Older Entries', "bonestheme")) ?></li>
					<li class="next-link"><?php previous_posts_link(_e('Newer Entries &raquo;', "bonestheme")) ?></li>
				</ul>
			</nav>
		<?php } ?>

		<?php 
		// Setup global and check if any faq posts are associated with this assignment
		global $hasFaqPosts;

		$faqArray = array('post_type' => array('jcah_faq','document'), 'assignments' => $catObj->name, 'doc_tag' => 'frequently-asked-questions');

		$hasFaqPosts = get_posts( $faqArray ); // lets check if there is any posts in the 'jcah_faq' post type.

	    if( !empty($hasFaqPosts) ) { // If we found some posts, update global
	    
	        $hasFaqPosts = TRUE;

	    }

	    // Setup global and check if any posts are assigned to training, including only jcah_faq
	    global $hasTrainingPosts;

		$trainingArray = array('post_type' => array('jcah_faq','document'), 'assignments' => $catObj->name, 'doc_tag' => 'training');

		$hasTrainingPosts = get_posts( $trainingArray ); // lets check if there is any posts in the 'jcah_faq' post type.

	    if( !empty($hasTrainingPosts) ) { // If we found some posts, update global
	    
	        $hasTrainingPosts = TRUE;

	    }
    	?>
		
		<?php wp_reset_postdata(); //Restore original Post Data ?>
		
	</div> <!-- end #main -->

	<?php get_sidebar(); // sidebar 1 ?>

</div> <!-- end #content -->

<?php get_footer(); ?>
