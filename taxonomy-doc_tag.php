<?php get_header(); $currentPage = 'taxonomy_doc_tag'; $assignmentSessionVar = $_SESSION['assignment']; ?>

<?php check_current_user_assignment_permissions($assignmentSessionVar); ?>

<?php global $docCatObj; $docCatObj = $wp_query->queried_object; ?>

<?php $args = array('post_type' => 'document', 'doc_tag' => $docCatObj->slug, 'assignments' => $assignmentSessionVar, 'paged' => $paged); ?>

<?php $doc = new WP_Query( $args );?>

<div id="content" class="clearfix row-fluid">

	<div id="main" class="span8 clearfix" role="main">

		<!-- Breadcrumb -->
		<small style="font-size:12px;">
			
			<ul class="breadcrumb">
		    
		        <li><a href="/">HOME</a> <span class="divider">/</span></li>
		    
		        <li><a href="?assignments=<?php echo $assignmentSessionVar; ?>"><?php echo $assignment_name = strSantizeTagBreanCrumb($_SESSION['assignment']);?></a> <span class="divider">/</span></li>
		    
		        <li class="active"><?php echo $doc_tag_name = strtoupper($docCatObj->name); ?></li>
	    	
	    	</ul>
	    
	    </small>

	    <?php printSanitizedPageTitle($docCatObj->name); ?>

		<?php if ( $doc->have_posts() ) : while ( $doc->have_posts() ) : $doc->the_post();?>
			
			<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" style="margin-bottom:0px !important" title="<?php the_title(); ?>">
			
				<header id="<?php echo $tag_list = commaSeparatedTagList($doc->post->ID, '' ,'doc_tag', ' '); ?>" class="doc-description">

					<h4><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h4>
				
				</header> <!-- end article header -->
				
				<section class="post_content">
					
					<?php $desc = get_field("description"); echo (!empty($desc) ? '<div>' . $desc . '</div>' : '') ?>

					<small class="muted">Updated <?php echo $date = formatModifiedDate($doc->post->post_modified); ?></small>

					<?php list_jcah_document_revisions($doc->post->ID); ?>
				
				</section> <!-- end article section -->
				
				<!-- <footer>
				
				</footer> --> <!-- end article footer -->
			
			</article> <!-- end article -->

			<?php $tags[] = get_the_terms($doc->post->ID, 'doc_tag'); ?>
	
		<?php endwhile; ?> 

			<?php if (function_exists('jcah_page_navi')) { // if expirimental feature is active ?>
						
				<?php jcah_page_navi('','',$doc ); // use the page navi function ?>
				
			<?php } else { // if it is disabled, display regular wp prev & next links ?>
				<nav class="wp-prev-next">
					<ul class="clearfix">
						<li class="prev-link"><?php next_posts_link(_e('&laquo; Older Entries', "bonestheme")) ?></li>
						<li class="next-link"><?php previous_posts_link(_e('Newer Entries &raquo;', "bonestheme")) ?></li>
					</ul>
				</nav>
			<?php } ?>

		<?php else: ?> <!-- end loop -->
			<span class="label label-inverse"><?php _e('No Documents are associated with this tag'); ?></span>
		<?php endif; ?>
		
		<?php wp_reset_postdata(); //Restore original Post Data ?>
		
	</div> <!-- end #main -->

	<?php get_sidebar(); // sidebar 1 ?>

</div> <!-- end #content -->

<?php get_footer(); ?>
