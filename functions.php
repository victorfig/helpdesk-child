<?php
// Custom JCAH Globals
global $tags;
global $currentPage;

// Custom JCAH Code - DO NOT REMOVE ANY INCLUDES BELOW
require_once('custom-code/jcahUserProfileField.php');
require_once('custom-code/jcahDocRevision.php');
require_once('custom-code/jcahAssignments.php');
require_once('custom-code/jcahDashboardScreenLayout.php');
require_once('custom-code/jcahCustomPostTypes.php');
require_once('custom-code/jcahStringFunc.php');
require_once('custom-code/jcahPreGetPosts.php');
require_once('custom-code/jcahCustomRolesCap.php');
require_once('custom-code/jcahSiteMapGen.php');
require_once('custom-code/JCAHPDF.class.php');
require_once('custom-code/jcahJsLibraries.php');

// Filters
add_filter('show_admin_bar', '__return_false'); // Force Admin Bar to hide for all users
add_filter('pre_option_link_manager_enabled', '__return_true'); // Enable WordPress Built-in Link Manager

// Generating PDFS
function get_jcah_wp_content($source) {
	
	$upload_dir = wp_upload_dir();
	
	return $upload_dir[$source];

}

/* The include below requires the Custom Fields plugin to be installed
 * To edit: go into Custom Fields plugin and change publish status of the 
 * custom field to Public, make necessary changes and finally export to PHP
 *
 * Once export is completed and changes have been tested, update the Publish
 * status of the custom field back to Draft, otherwise two fields will appear
 * in theme.
 */
require_once('custom-code/jcahDocCustomFields.php'); // DO NOT REMOVE


add_action( 'admin_menu' , 'remove_post_custom_fields' );
/**
 * Hides specific metaboxes from back-end
 *
 * @author Victor Figueroa
 * @param  none
 * @return none
 */
function remove_post_custom_fields() {
	remove_meta_box( 'formatdiv', 'post', 'normal' );
	remove_meta_box( 'postexcerpt', 'post', 'normal' );
	remove_meta_box( 'authordiv', 'post', 'normal' );
	remove_meta_box( 'commentstatusdiv', 'post', 'normal' );
	remove_meta_box( 'trackbacksdiv', 'post', 'normal' );
}


add_action( 'after_setup_theme', 'childtheme_remove_filters' );
/**
 * Removes filters set by parent theme
 *
 * @author Victor Figueroa
 * @param  none
 * @return none
 */
function childtheme_remove_filters(){
	// Remove filter that applies .lead class to first paragraph in single template
	remove_filter('the_content', 'first_paragraph');
	remove_filter('excerpt_more', 'bones_excerpt_more');
}


add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );
/**
 * Change default (55 words) excerpt length
 *
 * @author Victor Figueroa
 * @param  none
 * @return int Length of excerpt
 */
function custom_excerpt_length( $length ) {
	
	return 20;

}


add_filter('excerpt_more', 'new_excerpt_more');
/**
 * Displays the excerpt of the current post with the "[...]" 
 *
 * @author Victor Figueroa
 * @param  none
 * @return string Elipsis
 */
function new_excerpt_more( $more ) {
	
	return '...';

}


/**
 * Query current-user role-scoper id and return the names in an array
 *
 * @author Victor Figueroa
 * @param  none
 * @return array $role_names Array of group name of ID's provided
 */
function query_user_rs_assinged_group_name() {
	
	global $current_rs_user;
	
	global $wpdb;
	
	$rs_group_ids = $current_rs_user->groups;

	foreach($rs_group_ids as $key => $value) {
	
		$role_names[] = $wpdb->get_var($wpdb->prepare(
					"
						SELECT group_name
	  					FROM wp_groups_rs wprs
	 					WHERE wprs.ID = %d
	 				",
	 				$key
	 	) );
	
	 }

	 return $role_names;
}


/**
 * Provide an array of category IDs from an array of category slugs
 *
 * @author Victor Figueroa
 * @param  array $slug_array Array of category slugs
 * @return array $slug_ids Array of category IDs
 */
function returnCategoryIdArrayFromSlugArray($slug_array) {
	
	foreach($slug_array as $slug){
	
		$catObj = get_category_by_slug($slug);

		$slug_ids[] = $catObj->term_id;
	
	}
	
	return $slug_ids;
}


/**
 * Provide a single category id from given slug name
 *
 * @author Victor Figueroa
 * @param  string $strSlug Category slug name
 * @param  boolean $printCat Indicate if it should be printed (it will by default)
 * @return string $catName Category name
 */
function returnSingleCatIdFromSlugString($strSlug,$printCat = true) {
	
	$catObj = get_category_by_slug($strSlug);

	$slug_id = $catObj->term_id;
	
	if($printCat == false) {
	
		return $slug_id;
	
	}
	
	echo $slug_id;
}


/**
 * Provides the name of category id given
 *
 * @author Victor Figueroa
 * @param  int Category number / id
 * @return string $catName Category name
 */
function returnSingleSlugFromCatId($slugId) {
	
	return $catName = get_the_category_by_ID($slugId);

}


/**
 * Return an array of category slug names
 *
 * @author Victor Figueroa
 * @param  stdClass Object $catObj Category standard Class Object
 * @param  string $slug Category slug
 * @return boolean false
 */
function return_array_of_category_slugs($catObj,$slug) {
	
	foreach($catObj as $cat) {
	
		$categories[] = $cat->slug;
	
	}

	if(in_array($slug,$categories)) {
	
		echo strSantizeTagBreanCrumb($slug);
	
	}
	
	return false;
}


add_filter('login_url', 'change_login_url');
/**
 * Adds a redirect url to the homepage to $logouturl
 *
 * @author Victor Figueroa
 * @param  none
 * @return string home_url() Amended with redirect url
 */
function change_login_url() {
    
    return home_url('/');

}


add_filter('logout_url', 'jcah_logout_redirect_url', 10, 2);
/**
 * Adds a redirect url to the homepage to $logouturl
 *
 * @author Victor Figueroa
 * @param  string $logouturl Existing logouturl
 * @return string $logouturl Amended with redirect url
 */
function jcah_logout_redirect_url( $logouturl ) {
	
	return $logouturl . '&amp;redirect_to=' . urlencode( get_option( 'siteurl' ) );
 
}

// Numeric Page Navi (built into the theme by default)
function jcah_page_navi($before = '', $after = '', $custom_query = '') {
	global $wpdb, $wp_query;

    //Check for custom query variable, if set, assign to navi_query, if not, assign main wp_query to navi_query
    if (isset($custom_query) && $custom_query != '') {
        $wp_query = $custom_query;
    } else {
        $wp_query = $wp_query;
    }

	$request = $wp_query->request;
	$posts_per_page = intval(get_query_var('posts_per_page'));
	$paged = intval(get_query_var('paged'));
	$numposts = $wp_query->found_posts;
	$max_page = $wp_query->max_num_pages;
	if ( $numposts <= $posts_per_page ) { return; }
	if(empty($paged) || $paged == 0) {
		$paged = 1;
	}
	$pages_to_show = 7;
	$pages_to_show_minus_1 = $pages_to_show-1;
	$half_page_start = floor($pages_to_show_minus_1/2);
	$half_page_end = ceil($pages_to_show_minus_1/2);
	$start_page = $paged - $half_page_start;
	if($start_page <= 0) {
		$start_page = 1;
	}
	$end_page = $paged + $half_page_end;
	if(($end_page - $start_page) != $pages_to_show_minus_1) {
		$end_page = $start_page + $pages_to_show_minus_1;
	}
	if($end_page > $max_page) {
		$start_page = $max_page - $pages_to_show_minus_1;
		$end_page = $max_page;
	}
	if($start_page <= 0) {
		$start_page = 1;
	}
		
	echo $before.'<div class="pagination"><ul class="clearfix">'."";
	if ($paged > 1) {
		$first_page_text = "&laquo";
		echo '<li class="prev"><a href="'.get_pagenum_link().'" title="First">'.$first_page_text.'</a></li>';
	}
		
	$prevposts = get_previous_posts_link('&larr; Previous');
	if($prevposts) { echo '<li>' . $prevposts  . '</li>'; }
	else { echo '<li class="disabled"><a href="#">&larr; Previous</a></li>'; }
	
	for($i = $start_page; $i  <= $end_page; $i++) {
		if($i == $paged) {
			echo '<li class="active"><a href="#">'.$i.'</a></li>';
		} else {
			echo '<li><a href="'.get_pagenum_link($i).'">'.$i.'</a></li>';
		}
	}
	echo '<li class="">';
	next_posts_link('Next &rarr;');
	echo '</li>';
	if ($end_page < $max_page) {
		$last_page_text = "&raquo;";
		echo '<li class="next"><a href="'.get_pagenum_link($max_page).'" title="Last">'.$last_page_text.'</a></li>';
	}
	echo '</ul></div>'.$after."";
}