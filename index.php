<?php get_header(); ?>
			
			<?php

			/* $category_array is also used to indicate which post NOT to display in other loops on this page */
			$category_array = return_ids_of_category_array(array('Homepage Slider')); // Get id of categories used in slider

			$use_carousel = of_get_option('showhidden_slideroptions');
			if ($use_carousel) {
				global $post;
				$tmp_post = $post;
				$show_posts = of_get_option('slider_options');
				$args = array( 'numberposts' => $show_posts, 'category__in' => $category_array ); // set this to how many posts you want in the carousel
				$myposts = get_posts( $args );
				$post_num = 0;
				$slide_num = 0;
				$isFirst = true;
			?>
				<?php if(!empty($myposts)) { ?>
					<div id="myCarousel" class="carousel slide hp-slider-height">
						
						<?php
						// If post assigned to Homepage Slider is missing featured image then invoke error by setting variable
						foreach($myposts as $key => $value) {
							if(!has_post_thumbnail($value->ID) && !isset($freezeSlider)) { // If post does not have image assign
								$freezeSlider = 'yes';
							}
						}
						?>

					    <!-- Carousel items -->
					    <div class="carousel-inner hp-slider-height">

					    	<?php
							foreach( $myposts as $post ) :	setup_postdata($post);
								if(!has_post_thumbnail($post->ID)) {
									echo '<br /><div class="alert alert-error">
									        <h4 class="alert-heading">Error: "' . $post->post_title . '" post is missing the featured image!</h4>
									        <br />
									        <p>The slider will not work until this is fixed.</p>
									      </div>';
								} else {
									$post_num++;
									$post_thumbnail_id = get_post_thumbnail_id();
									$featured_src = wp_get_attachment_image_src( $post_thumbnail_id, 'wpbs-featured-carousel' );
								}
							?>

						    <div class="<?php if($post_num == 1){ echo 'active'; } ?> item hp-slider-height">
						    	<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail( 'wpbs-featured-carousel' ); ?></a>

							   	<div class="carousel-caption">

					                <h4><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h4>
					                <p>
					                	<?php
					                	if(has_post_thumbnail($post->ID)) { // Only run code below if post has thumbnail
					                		$excerpt_length = 100; // length of excerpt to show (in characters)
					                		$the_excerpt = get_the_excerpt(); 
					                		if($the_excerpt != ""){
					                			$the_excerpt = substr( $the_excerpt, 0, $excerpt_length );
					                			echo $the_excerpt . '... ';
					                	?>
					                	<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>" class="btn btn-primary">Read more &rsaquo;</a>
					                		<?php } ?>
					                	<?php } ?>
					                </p>

				                </div>
						    </div>

						    <?php endforeach; ?>
							<?php $post = $tmp_post; ?>

					    </div>

					    <!-- Carousel nav -->
					    <?php if(!is_admin() && !isset($freezeSlider)): // Only display slider navigation when no error with posts?>
						    <a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
						    <a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
						<?php endif; ?>
				    </div>
				<?php } else { ?>
					<div class="alert alert-error">
				        <h4 class="alert-heading">Error:</h4>
				        <p>Homepage Slider is enabled but no posts were assigned! The slider will not work until this is fixed.</p>
				    </div>
				<?php } ?>
			<?php } else { ?>
		    	<div class="clearfix row-fluid">
		    		<div class="hero-unit">
		    			<h1><?php bloginfo('title'); ?></h1>
		    			<p><?php bloginfo('description'); ?></p>
		    		</div>
		    	</div>
		    <?php }// ends the if use carousel statement ?>

		    <?php wp_reset_postdata();?>
			
			<div id="content" class="clearfix row-fluid">
				
				<div id="main" class="span8 clearfix" role="main">

					<h3 class="hp-section-title">News</h3>

					<hr class="page-title-hr"/>

					<?php $slug_array = array('news'); // Add category slug to display in homepage news ?>

					<?php $slug_id_array = returnCategoryIdArrayFromSlugArray($slug_array); ?>
					
					<?php $args = array('post_type' => array('post'), 'category__not_in' => $category_array, 'category__in' => $slug_id_array, 'posts_per_page' => '4');  ?>

					<?php $q = new WP_Query( $args );?>

					<?php if ($q->have_posts()) : while ($q->have_posts()) : $q->the_post(); ?>

						<?php $strToSearch = get_the_content(); $strJwplayer = 'jwplayer'; $strVideo = 'video'; ?>						

						<div class="featurette" title="<?php the_title(); ?>">

							<?php the_post_thumbnail( 'thumbnail', array('class' => 'featurette-image pull-left thumbnail-right-padding' ) ); ?>

							<?php if(false !== stripos($strToSearch,$strVideo) || false !== stripos($strToSearch,$strJwplayer) && has_post_thumbnail()): ?>

								<div style="position:absolute;"><a href="#postVideo-<?php the_ID()?>" data-toggle="modal"><img src="../wp-content/uploads/2014/01/play-button-overlay-sm.png" width="150" /></a></div>
							
							<?php endif; ?>

							<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">
									
								<header id="<?php echo $tag_list = commaSeparatedTagList($q->post->ID, '' ,'category', ' '); ?>">

									<h4 class="article-title"><a href="<?php the_permalink();?>&cat=<?php returnSingleCatIdFromSlugString('news');?>" title="<?php the_title(); ?>"><?php the_title(); ?></a> </h4>
									
									<small class="small muted">Posted <?php echo $date = formatModifiedDate($q->post->post_modified);?>

										<?php if(!has_post_thumbnail()): ?>

											<?php if(false !== stripos($strToSearch,$strVideo) || false !== stripos($strToSearch,$strJwplayer)): ?>
											
												<a href="#postVideo-<?php the_ID()?>" data-toggle="modal"><i class="icon-facetime-video"></i></a>
										
											<?php endif;?>
										
										<?php endif;?>

									</small>
								
								</header> <!-- end article header -->
								
								<section class="post_content">
									
									<?php the_excerpt(); ?>
								
								</section> <!-- end article section -->

								<!-- Video modal -->
								<div id="postVideo-<?php the_ID(); ?>" style="overflow:hidden;width: auto;height: auto" class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
								
									<div class="modal-header">
										
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><a href="#" onclick="jwplayer().stop();"><i class="icon-remove-sign icon-white"></i></a></button>
									
										<h3 id="myModalLabel"><?php the_title(); ?></h3>
									
									</div>

									<?php if(false !== stripos($strToSearch,$strJwplayer)): ?>

										<?php preg_match('/\[(.*?)\]/',$strToSearch, $matches);?>

										<?php $jwplayer = $matches[0]; ?>
									
										<?php echo JWP6_Shortcode::the_content_filter($jwplayer); ?>

									<?php endif;?>
								
								</div>
							
							</article> <!-- end article -->

						</div>

					<?php endwhile; ?>	

					<?php if (count($q->posts) >= 4) { // if expirimental feature is active ?>
					
						<a href="?cat=<?php echo $slug_id_array[0]; ?>" class="pull-right">More news articles &#187;</a>
					
					<?php } ?>		
					
					<?php else : ?>
					
					<article id="post-not-found">
					
					    <header>
					
					    	<h1><?php _e("Not Found", "bonestheme"); ?></h1>
					
					    </header>
					
					    <section class="post_content">
					
					    	<p><?php _e("Sorry, but the requested resource was not found on this site.", "bonestheme"); ?></p>
					
					    </section>
					
					    <footer>
					
					    </footer>
					
					</article>
					
					<?php endif; ?>
			
				</div> <!-- end #main -->

				<?php wp_reset_postdata(); //Restore original Post Data ?>
    
				<?php if ( is_user_logged_in()  ): ?>
					
					<div id="sidebar1" class="fluid-sidebar sidebar span4 pull-right" role="complementary">
					
						<h3 class="widgettitle template-section-title" style="margin-top:-1px;">Recently updated</h4>
					
						<hr class="page-title-hr"/>
					
						<?php $recent_update_docs_post_per_page = 5; // Used on query and more link after query ?>
					
						<?php $recent_update_docs_args = array('post_type' => array('document'), 'category__not_in' => $category_array, 'posts_per_page' => $recent_update_docs_post_per_page, 'orderby' => 'modified');  ?>

						<?php $recent_update_docs = new WP_Query( $recent_update_docs_args ); ?>

						<?php if($recent_update_docs->have_posts()) : while ($recent_update_docs->have_posts()) : $recent_update_docs->the_post(); ?>
					
							<dl style="margin-bottom:-10px">
					
							<a href="<?php the_permalink(); ?>"><i class="icon-download-alt" style="opacity:0.60"></i></a> <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
					
							<dd><small><?php $revDesc = $recent_update_docs->post->post_excerpt; echo (!empty($revDesc)) ? $revDesc : ''; ?></small></dd>
					
							<dd><small class="muted"><em>Modified on <?php echo $date = formatModifiedDate($recent_update_docs->post->post_modified); ?></em></small></dd>
					
							</dl>
					
						<?php endwhile; ?>
					
						<br />
					
							<?php echo ($recent_update_docs->post_count > $recent_update_docs_post_per_page) ? '<p><small><a href="document-revisions/">More recent update files...</a></small></p>' : '';?>
					
						<?php wp_reset_postdata(); //Restore original Post Data ?>
					
						<?php else:  ?>
					
						  <p><?php _e( 'No documents uploaded yet.' ); ?></p>
					
						<?php endif; ?>
					
					</div>
				
				<?php endif; ?>

				<?php get_sidebar(); // sidebar 1 ?>
			
			</div> <!-- end #content -->

<?php get_footer(); ?>