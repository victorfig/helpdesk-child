<?php get_header(); ?>
			
			<div id="content" class="clearfix row-fluid">
			
				<div id="main" class="span12 clearfix" role="main">

					<article id="post-not-found" class="clearfix">
						
						<header>

							<div class="hero-unit">

							<h2>The 
								<?php
									# Setup variables
									$adminemail = get_option('admin_email'); // The administrator email address
									$website = get_bloginfo('url');	// Get site url
									$websitename = get_bloginfo('name'); // Sets the site's name

									if (!isset($_SERVER['HTTP_REFERER'])) {
										echo "page or file you are looking for"; // starts assembling an output
										// $casemessage = $website.$_SERVER['REQUEST_URI'];
									} elseif (isset($_SERVER['HTTP_REFERER'])) {
										// this will help the user find what they want, and email me of a bad link
										echo "clicked a link to"; #now the message says You clicked a link to...
									    // setup a message to be sent to me
										$failuremess = "A user tried to go to $website"
									        .$_SERVER['REQUEST_URI'] . " and received a 404 (page not found) error. ";
										$failuremess .= "It wasn't their fault, so try fixing it.  
									        They came from ".$_SERVER['HTTP_REFERER'];
										mail($adminemail, "Bad Link To ".$_SERVER['REQUEST_URI'],
									        $failuremess, "From: $websitename <noreply@$website>"); // email you about problem
										$casemessage = "An administrator has been emailed 
									        about this problem, too."; // set a friendly message
									}
								?> 
								cannot be found.</h2>
								
								<?php _e("We recommend you start from the <a href='/'>home page</a>."); ?>
															
							</div>
													
						</header> <!-- end article header -->
					
						<!-- <section class="post_content">
							
							<p><?php _e("Whatever you were looking for was not found, but maybe try looking again or search using the form below.","bonestheme"); ?></p>

							<div class="row-fluid">
								<div class="span12">
									<?php #get_search_form(); ?>
								</div>
							</div>
					
						</section> --> <!-- end article section -->
						
						<footer>
							
						</footer> <!-- end article footer -->
					
					</article> <!-- end article -->
			
				</div> <!-- end #main -->
    
			</div> <!-- end #content -->

<?php get_footer(); ?>