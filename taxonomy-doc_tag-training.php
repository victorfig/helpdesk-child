<?php get_header(); $currentPage = 'taxonomy_doc_tag_training'; ?>

<?php $catObj = $wp_query->queried_object; $assignmentSessionVar = $_SESSION['assignment']; $faqTag = 'frequently-asked-questions'; $trainingTag = 'training';?>

<?php $args = array('post_type' => array('document','jcah_faq'), 'doc_tag' => $trainingTag, 'assignments' => $assignmentSessionVar, 'paged' => $paged, ); ?>

<?php $faq = new WP_Query( $args ); ?>

<?php check_current_user_assignment_permissions($assignmentSessionVar); ?>

<div id="content" class="clearfix row-fluid">
			
	<div id="main" class="span8 clearfix" role="main">

		<!-- Breadcrumb -->
		<small style="font-size:12px;">
			
			<ul class="breadcrumb">
		    
		        <li><a href="/">HOME</a> <span class="divider">/</span></li>
		    
		        <li><a href="?assignments=<?php echo $assignmentSessionVar; ?>"><?php echo $assignment_name = strSantizeTagBreanCrumb($assignmentSessionVar);?></a> <span class="divider">/</span></li>
		    
		        <?php if(empty($secondTag)):?>
		    
		        	<li class="active"><?php echo $doc_tag_name = strtoupper($catObj->name); ?></li>
		    
		        <?php endif;?>

		        <?php if(!empty($secondTag)):?>
		    
		        	<li class="active"><?php echo $doc_tag_name = strtoupper($catObj->name); ?></a> <span class="divider">/</span></li>
		    
		        	<li class="active"><?php echo $second_doc_tag_name = strtoupper($secondTag); ?></li>
		    
		        <?php endif;?>
	    	
	    	</ul>
	    
	    </small>

	    <?php printSanitizedPageTitle($assignmentSessionVar); ?>
		
		<?php if ( $faq->have_posts() ) : while ( $faq->have_posts() ) : $faq->the_post();?>

				<?php $tags[] = get_the_terms($faq->post->ID, 'doc_tag'); ?>
		
		<?php endwhile; else: ?> <!-- end loop -->
		
			<span class="label label-inverse"><?php _e('No tags available'); ?></span>
		
		<?php endif; ?>
		
		<?php rewind_posts(); ?>

		<?php if(!empty($faq->posts)): ?>

			<?php $postTags = createSimpleArrayFromMultiDimensionalArray($tags); #print_r($postTags);?>

			<?php foreach($postTags as $postTag) { $thisPostTags[]=$postTag['slug']; }?>

				<?php foreach($thisPostTags as $thisPostTag):?>

					<?php if($thisPostTag != $faqTag && $thisPostTag != $trainingTag):?>
							
							<a name="<?php echo $thisPostTag; ?>"></a>
							
							<div class="accordion" id="accordion">
							
								<div class="accordion-group faq-remove-borders">
							
									<div class="accordion-heading" title="<?php echo strSantizeTagStrings($thisPostTag); ?> FAQs">
							
										<strong class="accordion-toggle" data-toggle="non-collapse" data-parent="#accordion" href="#collapse-<?php echo $thisPostTag;?>">
							
											<?php echo strSantizeTagStrings($thisPostTag); ?> <a href="#top"><i class="icon-arrow-up faq-up-arrow"></i></a>
							
										</strong>
							
									</div>
							
									<div id="collapse-<?php echo $thisPostTag;?>" class="accordion-body collapse in faq-questions-box" >
							
										<div class="accordion-inner faq-remove-borders">

											<?php if ( $faq->have_posts() ) : while ( $faq->have_posts() ) : $faq->the_post();?>

												<?php $strToSearch = get_the_content(); $strJwplayer = 'jwplayer'; $strVideo = 'video'; ?>

												<?php if(is_object_in_term($faq->post->ID, 'doc_tag', $thisPostTag)):?>

													<?php $strToSearch = get_the_content(); $strJwplayer = 'jwplayer'; $strVideo = 'video'; ?>						

													<div class="featurette" title="<?php the_title(); ?>">

														<?php if($faq->post->post_type == 'jcah_faq'): ?>

															<?php if(false !== stripos($strToSearch,$strVideo) || false !== stripos($strToSearch,$strJwplayer)): ?>

																<div class="pull-left jcah-training-tpl-icons"><a href="#postVideo-<?php the_ID()?>" data-toggle="modal"><img src="../wp-content/uploads/2014/01/play-button-overlay-sm.png" width="150" /></a></div>

															<?php else: ?>

																<div class="pull-left jcah-training-tpl-icons"><a href="#postModal-<?php the_ID()?>" data-toggle="modal"><img src="../wp-content/uploads/2014/01/Icon-Document.png" width="150" /></a></div>

															<?php endif; ?>

														<?php elseif($faq->post->post_type == 'document'): ?>

															<div class="pull-left jcah-training-tpl-icons"><a href="<?php the_permalink()?>"><img src="../wp-content/uploads/2014/01/ACP_PDF-2_file_document.png" width="150" /></a></div>

														<?php endif; ?>


														<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">
																
															<header id="<?php echo $tag_list = commaSeparatedTagList($q->post->ID, '' ,'category', ' '); ?>">

																<?php if($faq->post->post_type == 'jcah_faq'): ?>

																	<?php if(false !== stripos($strToSearch,$strVideo) || false !== stripos($strToSearch,$strJwplayer)): ?>

																		<div><a href="#postVideo-<?php the_ID()?>" data-toggle="modal"><?php the_title(); ?></a></div>
																	
																	<?php else: ?>

																		<div><a href="#postFaq-<?php the_ID()?>" data-toggle="modal"><?php the_title(); ?></a></div>
																		
																	<?php endif;?>

																<?php elseif($faq->post->post_type == 'document'): ?>

																	<div><a href="<?php the_permalink()?>"><?php the_title(); ?></a></div>

																<?php endif;?>

																<small class="small muted">Posted <?php echo $date = formatModifiedDate($faq->post->post_modified);?></small>
															
															</header> <!-- end article header --> <!-- end article section -->

															<!-- Video modal for title -->
															<?php if($faq->post->post_type == 'jcah_faq'): ?>

																<div id="postVideo-<?php the_ID(); ?>" style="overflow:hidden;width: auto;height: auto" class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
																
																	<div class="modal-header">
																		
																		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><a href="#" onclick="jwplayer().stop();"><i class="icon-remove-sign icon-white"></i></a></button>
																	
																		<h3 id="myModalLabel"><?php the_title(); ?></h3>
																	
																	</div>

																	<div class="modal-body">

																		<?php the_content(); ?>

																	</div>

																	<div class="modal-footer">
																	
																		<button class="btn" data-dismiss="modal" aria-hidden="true"><a href="#" onclick="jwplayer().stop();" style="color:#fff;text-decoration:none;">Close</a></button>
																	
																	</div>
																
																</div>

																<!-- Modal with no video -->
																<div id="postFaq-<?php the_ID()?>" class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
																	
																	<div class="modal-header">
																	
																		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><a href="#" onclick="jwplayer().stop();"><i class="icon-remove-sign icon-white"></i></a></button>
																	
																		<h3 id="myModalLabel"><?php the_title(); ?></h3>
																	
																	</div>
																	
																	<div class="modal-body">
																	
																		<?php the_content(); ?>
																	
																	</div>
																	
																	<div class="modal-footer">
																	
																		<button class="btn" data-dismiss="modal" aria-hidden="true"><a href="#" onclick="jwplayer().stop();" style="color:#fff;text-decoration:none;">Close</a></button>
																	
																	</div>
																
																</div>

															<?php endif; ?>
														
														</article> <!-- end article -->

													</div>
										
												<?php endif;?> <!-- End of Term Check -->
										
											<?php endwhile; else: ?> <!-- end while loop -->
										
												<span class="label label-inverse"><?php _e('No Documents associated with this tag'); ?></span>
										
											<?php endif; // end loop if statement?>
										
										</div> <!-- End accordion-inner div -->
									
									</div> <!-- End collapse div -->
								
								</div> <!-- End accordion-group div -->
							
							</div> <!-- End accordion div -->

					<?php endif;?> <!-- End if thisPostTag != faq -->

				<?php endforeach; ?> <!-- End foreach thisPostTag -->
			
			<?php wp_reset_postdata(); //Restore original Post Data ?>

		<?php endif;?>
	
	</div> <!-- end #main -->

	<?php get_sidebar(); // sidebar 1 ?>

</div> <!-- end #content -->

<?php get_footer(); ?>
