<?php
/*
Template Name: Document Revisions
*/
get_header();

$catObj = $wp_query->queried_object;

/* $category_array is also used to indicate which post NOT to display in other loops on this page */
$category_array = return_ids_of_category_array(array('Homepage Slider')); // Get id of categories used in slider
?>

<?php $args = array('post_type' => 'document', 'orderby' => 'modified', 'category__not_in' => $category_array); ?>

<?php $doc_revisions = new WP_Query( $args );?>

<div id="content" class="clearfix row-fluid">

	<div id="main" class="span8 clearfix" role="main">

		<h1><?php echo $catObj->post_title; ?></h1>

		<?php if ( $doc_revisions->have_posts() ) : while ( $doc_revisions->have_posts() ) : $doc_revisions->the_post(); $tags[] = get_the_tags();?>

			<article id="post-<?php the_ID(); ?>" title="<?php the_title(); ?>" <?php post_class('clearfix'); ?> role="article" style="margin-bottom:0px !important">

				<header id="<?php echo $tag_list = commaSeparatedTagList($post->post->ID, '' ,'doc_tag', ' '); ?>">

					<div><h3><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3></div>
				
				</header> <!-- end article header -->
				
				<section class="post_content">
					
					<?php $desc = get_field("description"); echo (!empty($desc) ? '<h6 style="margin-top:-5px;margin-bottom:-2px">' . $desc . '</h6>' : '') ?>
				
				</section> <!-- end article section -->
				
				<footer>
				
				<small style="color:#b4bcc2;">Updated On: <?php echo $date = formatModifiedDate($doc_revisions->post->post_modified); ?></small>
				
				<?php list_jcah_document_revisions($doc_revisions->post->ID); ?>
				
				</footer> <!-- end article footer -->
			
			</article> <!-- end article -->

			<?php $tags[] = get_the_terms($doc_revisions->post->ID, 'doc_tag'); ?>
	
		<?php endwhile; else: ?> <!-- end loop -->
	
			<p><?php _e('No Documents added.'); ?></p>

		<?php endif; ?>
		
		<?php wp_reset_postdata(); //Restore original Post Data ?>
		
	</div> <!-- end #main -->

	<?php get_sidebar(); // sidebar 1 ?>

</div> <!-- end #content -->

<?php get_footer(); ?>
