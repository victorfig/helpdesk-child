<?php 
get_header(); ?>

<?php $catObj = $wp_query->queried_object; ?>

<?php $args = array('post_type' => 'document', 'category_name' => $catObj->slug); ?>

<?php $doc = new WP_Query( $args );?>

<div id="content" class="clearfix row-fluid">

	<div id="main" class="span8 clearfix" role="main">

		<h1><?php echo $catObj->name; ?></h1>

		<?php if ( $doc->have_posts() ) : while ( $doc->have_posts() ) : $doc->the_post(); $tags[] = get_the_tags();?>

			<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" style="margin-bottom:0px !important">
			
				<header id="<?php echo $tag_list = commaSeparatedTagList($post->post->ID); ?>">

					<div><h3><?php the_title(); ?></h3></div>
				
				</header> <!-- end article header -->
				
				<section class="post_content">
					
					<h6 style="margin:0px;"><?php echo (!empty($doc->post->post_excerpt) ? $doc->post->post_excerpt : ''); ?></h6>
				
				</section> <!-- end article section -->
				
				<footer>

					<a class="btn btn-success btn-small" href="<?php echo get_permalink(); ?>">Download</a>

					<small style="color:#b4bcc2;">Updated On: <?php echo $date = formatModifiedDate($doc->post->post_modified); ?></small>
				
				</footer> <!-- end article footer -->
			
			</article> <!-- end article -->
	
		<?php endwhile; else: ?> <!-- end loop -->
	
			<p><?php _e('No Documents added.'); ?></p>

		<?php endif; ?>
		
		<?php wp_reset_postdata(); //Restore original Post Data ?>
		
	</div> <!-- end #main -->

	<?php get_sidebar(); // sidebar 1 ?>

</div> <!-- end #content -->

<?php get_footer(); ?>
