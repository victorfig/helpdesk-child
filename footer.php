			<footer role="contentinfo">
				
				<div id="inner-footer" class="clearfix">
		          
		          <p class="clearfix">&nbsp;</p>
		          
		          <div id="widget-footer" class="clearfix row-fluid" style="background-color:rgb(246,246,246); border-top:solid #2c3e50;">
		            
		            <div style="padding-left:15px;padding-top:15px;">
			            <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('footer1') ) : ?>
			            <?php endif; ?>
			            <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('footer2') ) : ?>
			            <?php endif; ?>
			            <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('footer3') ) : ?>
			            <?php endif; ?>
		            </div>
		          
		          </div>
					
					<nav class="clearfix">
						<?php bones_footer_links(); // Adjust using Menus in Wordpress Admin ?>
					</nav>
					
					<!-- <p class="pull-right" style="color:#ccc;">320press</p> -->
					<small class="clearfix">
						<p class="pull-right">All content on the helpdesk must be kept confidential.</p>

						<p class="attribution">&copy; <?php bloginfo('name'); ?></p>
					</small>
				
				</div> <!-- end #inner-footer -->
				
			</footer> <!-- end footer -->
		
		</div> <!-- end #container -->
				
		<!--[if lt IE 7 ]>
  			<script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
  			<script>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
		<![endif]-->

		<script type="text/javascript">
			jQuery(document).ready(function($) {
			    <?php if ( !is_admin() ): ?>
				    <?php if ( is_front_page() ): ?>
				    // Take care of rotating slider
					    $('.carousel').carousel({
					  		interval: 7000	
					  	});
					<?php endif; ?>
						
					<?php if ( !is_user_logged_in() ): ?>
						// Take care of login form
					  	// cache references to the input elements into variables
					    var passwordField = $('#user_pass');
					    var usernameField = $('#user_login');

						$(passwordField).focus(function(){
						  $(this).css('color','#000');
						  $(this).get(0).type='password';
						});

					    // When focus is placed on username field, remove placeholder and add black color
						usernameField.focus(function() {
					    	usernameField.css('color','#000');
					    });
			    
					<?php endif;?>
				
				<?php endif;?>
			});
		</script>
		
		<?php wp_footer(); // js scripts are inserted using this function ?>
	</body>

</html>