<?php get_header(); $currentPage = 'tag_template'?>

<?php global $newsCatObj; $newsCatObj = $wp_query->queried_object;?>

<div id="content" class="clearfix row-fluid">

	<div id="main" class="span8 clearfix" role="main">

		<!-- Breadcrumb -->
		<small style="font-size:12px;">
			
			<ul class="breadcrumb">
		    
		        <li><a href="/">HOME</a> <span class="divider">/</span></li>
		    
		        <li class="active"><a href="?cat=<?php returnSingleCatIdFromSlugString('news'); ?>">NEWS</a> <span class="divider">/</span></li>
		    
		        <li class="active"><?php echo $assignment_name = strSantizeTagBreanCrumb($_GET['tag']); ?></li>
	    	
	    	</ul>
	    
	    </small>

		<?php printSanitizedPageTitle($_GET['tag']); ?>   
	    
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			<?php $strToSearch = get_the_content(); $strJwplayer = 'jwplayer'; $strVideo = 'video'; ?>

			<div class="featurette">

				<?php the_post_thumbnail( 'thumbnail', array('class' => 'featurette-image pull-left thumbnail-right-padding' ) ); ?>

				<?php if(false !== stripos($strToSearch,$strVideo) || false !== stripos($strToSearch,$strJwplayer) && has_post_thumbnail()): ?>

					<div style="position:absolute;"><a href="#postVideo-<?php the_ID()?>" data-toggle="modal"><img src="../wp-content/uploads/2014/01/play-button-overlay-sm.png" width="150" /></a></div>
				
				<?php endif; ?>
			
				<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">
						
					<header id="<?php echo $tag_list = commaSeparatedTagList($q->post->ID, '' ,'category', ' '); ?>">

						<h4 class="article-title"><a href="<?php the_permalink() ?>&tag=<?php echo $_GET['tag']; ?>" rel="bookmark" title="<?php the_title_attribute(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h4>
						
						<small class="small muted"><?php _e("Posted", "bonestheme"); ?> <time datetime="<?php echo the_time('Y-m-j'); ?>" pubdate><?php the_date(); ?></time>

							<?php if(!has_post_thumbnail()): ?>

								<?php if(false !== stripos($strToSearch,$strVideo) || false !== stripos($strToSearch,$strJwplayer)): ?>
								
									<a href="#postVideo-<?php the_ID()?>" data-toggle="modal"><i class="icon-facetime-video"></i></a>
							
								<?php endif;?>
							
							<?php endif;?>
						
						</small>
					
					</header> <!-- end article header -->
					
					<section class="post_content">

						<?php the_excerpt(); ?>
					
					</section> <!-- end article section -->

					<!-- Video modal -->
					<div id="postVideo-<?php the_ID(); ?>" style="overflow:hidden;width: auto;height: auto" class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					
						<div class="modal-header">
						
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icon-remove-sign icon-white"></i></button>
						
							<h3 id="myModalLabel"><?php the_title(); ?></h3>
						
						</div>

						<?php if(false !== stripos($strToSearch,$strJwplayer)): ?>

							<?php preg_match('/\[(.*?)\]/',$strToSearch, $matches);?>

							<?php $jwplayer = $matches[0]; ?>
						
							<?php echo JWP6_Shortcode::the_content_filter($jwplayer); ?>

						<?php endif;?>
					
					</div>
				
				</article> <!-- end article -->

			</div>

			<?php $tags[] = get_the_terms($post->ID, 'post_tag'); ?>
			
			<?php endwhile; ?>

			<p>&nbsp;</p>
			
			<?php if (function_exists('jcah_page_navi')) { // if expirimental feature is active ?>
						
				<?php jcah_page_navi( ); // use the page navi function ?>
				
			<?php } else { // if it is disabled, display regular wp prev & next links ?>
				<nav class="wp-prev-next">
					<ul class="clearfix">
						<li class="prev-link"><?php next_posts_link(_e('&laquo; Older Entries', "bonestheme")) ?></li>
						<li class="next-link"><?php previous_posts_link(_e('Newer Entries &raquo;', "bonestheme")) ?></li>
					</ul>
				</nav>
			<?php } ?>
			
			<?php else : ?>
			
			<article id="post-not-found">
			    <header>
			    	<h1><?php _e("No Posts Yet", "bonestheme"); ?></h1>
			    </header>
			    <section class="post_content">
			    	<p><?php _e("Sorry, What you were looking for is not here.", "bonestheme"); ?></p>
			    </section>
			    <footer>
			    </footer>
			</article>
			
		<?php endif; ?>
		
	</div> <!-- end #main -->

	<?php get_sidebar(); // sidebar 1 ?>

</div> <!-- end #content -->

<?php get_footer(); ?>
