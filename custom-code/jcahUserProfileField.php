<?php
/**
 * Hide specific social media related user profile fields
 *
 * @author Victor Figueroa
 * @param  array $contactmethods global array
 * @return array $contactmethods global array
 */
function hide_profile_fields( $contactmethods ) {
	
    unset($contactmethods['aim']);
	
    unset($contactmethods['jabber']);
	
    unset($contactmethods['yim']);
	
    unset($contactmethods['url']);
	
    unset($contactmethods['description']);
	
    return $contactmethods;
}


/**
 * Remove option to change dashboard color theme
 *
 * @author Victor Figueroa
 * @param  none
 * @return none
 */
function admin_del_options() {
   
   global $_wp_admin_css_colors;
   
   $_wp_admin_css_colors = 0;
}


/**
 * Customize user-profile
 *
 * @author Victor Figueroa
 * @param  none
 * @return none
 */
function hide_personal_options(){
    global $current_user;
    $currentUserRoles = $current_user->roles;
?>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $('form#your-profile > h3').remove();
        $('form#your-profile').show();
        $('form#your-profile > h3:first').remove();
        $('form#your-profile > table:first').remove();

        //$(\'form#your-profile label[for=url], form#your-profile input#url\').remove();
        //$(\'form#createuser label[for=role], form#createuser select#role\').remove();
        //$(\'form#your-profile label[for=user_fb], form#your-profile input#user_fb\').remove();
        //$(\'form#your-profile label[for=user_tw], form#your-profile input#user_tw\').remove();
        //$(\'form#your-profile label[for=google_profile], form#your-profile input#google_profile\').remove();
        //$(\'form#createuser label[for=url], form#createuser input#url\').remove();

        $('form#your-profile label[for=description], form#your-profile textarea#description, form#your-profile span.description').remove();
        $('form#your-profile tr:contains("Website")').remove();
        $('form#your-profile tr:contains("Facebook")').remove();
        $('form#your-profile tr:contains("Twitter")').remove();
        $('form#your-profile tr:contains("Google Profile URL")').remove();

        $('form#your-profile h3:contains("Feed Privacy")').remove();
        $('form#your-profile table:contains("Secret Feed Key")').remove();

        <?php foreach($currentUserRoles as $currentUserRole):?>
            <?php if($currentUserRole == 'jcah_non_user_account'):?>
                $('#your-profile :input').attr('disabled', true);
            <?php endif; ?>
        <?php endforeach; ?>

        <?php if( (!current_user_can('install_themes')) ): ?>
            $('a:contains("Role Groups")').remove();
            $('#toplevel_page_rs-category-restrictions_t:contains("Restrictions")').remove();
            $('#toplevel_page_rs-general_roles:contains("Roles")').remove();
        <?php endif; ?>
    });
</script>
<?php
}

// Only invoke functions above if user is on the profile page
global $pagenow;
if ($pagenow == 'profile.php' || $pagenow == 'user-edit.php' || is_admin()) {

    // We are inside the user profile page... Do stuff...

    add_filter('user_contactmethods','hide_profile_fields',10,1);

    add_action('admin_head', 'admin_del_options');

    add_action('admin_footer','hide_personal_options');

}