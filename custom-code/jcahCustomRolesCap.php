<?php
// ONLY uncomment line below when changes are to corresponding function
#add_action( 'after_setup_theme', 'add_custom_jcah_capabilities' );
/**
 *  Add customized capabilities when theme is activeted.
 *  This will not run for each request. It will on theme activation.
 *  So if changes are made, please activate a different theme and re-activate
 *  this one to ensure changes take effect. Purpose of only running this
 *  on theme activation is to avoid affecting site performance by checking
 *  on each request. Changes made to this function will be saved to database. 
 *  No need to rerun on each request.
 *
 * @author Victor Figueroa
 * @param  none
 * @return none
 */
function add_custom_jcah_capabilities() {

    /* Get the jcah editor role. */
    $jcah_editor_role = get_role( 'jcah_editor' );

    /* If the jcah editor role exists, add required capabilities. */
    if ( !empty( $jcah_editor_role ) ) {

        /* FAQ Role management capabilities. */
        $jcah_editor_role->add_cap( 'publish_faqs' );
        $jcah_editor_role->add_cap( 'edit_faqs' );
        $jcah_editor_role->add_cap( 'edit_others_faqs' );
        $jcah_editor_role->add_cap( 'edit_published_faqs' );
        $jcah_editor_role->add_cap( 'delete_faqs' );
        $jcah_editor_role->add_cap( 'delete_others_faqs' );
        $jcah_editor_role->add_cap( 'delete_published_faqs' );
        $jcah_editor_role->add_cap( 'read_private_faqs' );
        $jcah_editor_role->add_cap( 'read_faqs' );
        $jcah_editor_role->add_cap( 'edit_faq' );
        $jcah_editor_role->add_cap( 'delete_faq' );
        $jcah_editor_role->add_cap( 'read_faq' );
        $jcah_editor_role->add_cap( 'read' );
        $jcah_editor_role->add_cap( 'level_7' );
        $jcah_editor_role->add_cap( 'level_6' );
        $jcah_editor_role->add_cap( 'level_5' );
        $jcah_editor_role->add_cap( 'level_4' );
        $jcah_editor_role->add_cap( 'level_3' );
        $jcah_editor_role->add_cap( 'level_2' );
        $jcah_editor_role->add_cap( 'level_1' );
        $jcah_editor_role->add_cap( 'level_0' );

        $jcah_editor_role->add_cap( 'delete_documents' );
        $jcah_editor_role->add_cap( 'delete_others_documents' );
        $jcah_editor_role->add_cap( 'delete_others_posts' );
        $jcah_editor_role->add_cap( 'delete_posts' );
        $jcah_editor_role->add_cap( 'delete_private_documents' );
        $jcah_editor_role->add_cap( 'delete_private_posts' );
        $jcah_editor_role->add_cap( 'delete_published_documents' );
        $jcah_editor_role->add_cap( 'delete_published_posts' );
        $jcah_editor_role->add_cap( 'edit_documents' );
        $jcah_editor_role->add_cap( 'edit_files' );
        $jcah_editor_role->add_cap( 'edit_others_documents' );
        $jcah_editor_role->add_cap( 'edit_others_posts' );
        $jcah_editor_role->add_cap( 'edit_posts' );
        $jcah_editor_role->add_cap( 'edit_private_documents' );
        $jcah_editor_role->add_cap( 'edit_private_posts' );
        $jcah_editor_role->add_cap( 'edit_published_documents' );
        $jcah_editor_role->add_cap( 'edit_published_posts' );
        $jcah_editor_role->add_cap( 'moderate_comments' );
        $jcah_editor_role->add_cap( 'publish_documents' );
        $jcah_editor_role->add_cap( 'publish_posts' );
        $jcah_editor_role->add_cap( 'read_document_revisions' );
        $jcah_editor_role->add_cap( 'read_documents' );
        $jcah_editor_role->add_cap( 'read_private_documents' );
        $jcah_editor_role->add_cap( 'read_private_posts' );
        $jcah_editor_role->add_cap( 'restrict_content' );
        $jcah_editor_role->add_cap( 'unfiltered_html' );
        $jcah_editor_role->add_cap( 'unfiltered_upload' );
        $jcah_editor_role->add_cap( 'upload_files' );
    }

    /* Get the jcah editor role. */
    $jcah_admin_role = get_role( 'jcah_admin' );

    /* If the jcah editor role exists, add required capabilities. */
    if ( !empty( $jcah_admin_role ) ) {

        /* FAQ Role management capabilities. */
        $jcah_admin_role->add_cap( 'publish_faqs' );
        $jcah_admin_role->add_cap( 'edit_faqs' );
        $jcah_admin_role->add_cap( 'edit_others_faqs' );
        $jcah_admin_role->add_cap( 'edit_published_faqs' );
        $jcah_admin_role->add_cap( 'delete_faqs' );
        $jcah_admin_role->add_cap( 'delete_others_faqs' );
        $jcah_admin_role->add_cap( 'delete_published_faqs' );
        $jcah_admin_role->add_cap( 'read_private_faqs' );
        $jcah_admin_role->add_cap( 'read_faqs' );
        $jcah_admin_role->add_cap( 'edit_faq' );
        $jcah_admin_role->add_cap( 'delete_faq' );
        $jcah_admin_role->add_cap( 'read_faq' );
        $jcah_admin_role->add_cap( 'read' );
        $jcah_admin_role->add_cap( 'level_7' );
        $jcah_admin_role->add_cap( 'level_6' );
        $jcah_admin_role->add_cap( 'level_5' );
        $jcah_admin_role->add_cap( 'level_4' );
        $jcah_admin_role->add_cap( 'level_3' );
        $jcah_admin_role->add_cap( 'level_2' );
        $jcah_admin_role->add_cap( 'level_1' );
        $jcah_admin_role->add_cap( 'level_0' );
        $jcah_admin_role->add_cap( 'delete_documents' );
        $jcah_admin_role->add_cap( 'delete_others_documents' );
        $jcah_admin_role->add_cap( 'delete_others_posts' );
        $jcah_admin_role->add_cap( 'delete_posts' );
        $jcah_admin_role->add_cap( 'delete_private_documents' );
        $jcah_admin_role->add_cap( 'delete_private_posts' );
        $jcah_admin_role->add_cap( 'delete_published_documents' );
        $jcah_admin_role->add_cap( 'delete_published_posts' );
        $jcah_admin_role->add_cap( 'edit_documents' );
        $jcah_admin_role->add_cap( 'edit_files' );
        $jcah_admin_role->add_cap( 'edit_others_documents' );
        $jcah_admin_role->add_cap( 'edit_others_posts' );
        $jcah_admin_role->add_cap( 'edit_posts' );
        $jcah_admin_role->add_cap( 'edit_private_documents' );
        $jcah_admin_role->add_cap( 'edit_private_posts' );
        $jcah_admin_role->add_cap( 'edit_published_documents' );
        $jcah_admin_role->add_cap( 'edit_published_posts' );
        $jcah_admin_role->add_cap( 'moderate_comments' );
        $jcah_admin_role->add_cap( 'publish_documents' );
        $jcah_admin_role->add_cap( 'publish_posts' );
        $jcah_admin_role->add_cap( 'read_document_revisions' );
        $jcah_admin_role->add_cap( 'read_documents' );
        $jcah_admin_role->add_cap( 'read_private_documents' );
        $jcah_admin_role->add_cap( 'read_private_posts' );
        $jcah_admin_role->add_cap( 'restrict_content' );
        $jcah_admin_role->add_cap( 'unfiltered_html' );
        $jcah_admin_role->add_cap( 'unfiltered_upload' );
        $jcah_admin_role->add_cap( 'upload_files' );
    }
}


add_filter('editable_roles', 'hide_builtin_roles');
/**
 *  
 *  Filters out the list of roles available to administrators. Custom
 *  roles were created, so to avoid confusion the built-in roles below
 *  will be removed because they're not being used.
 *
 * @author Victor Figueroa
 * @param  array global $all_roles
 * @return array global $all_roles
 */
function hide_builtin_roles($all_roles) {

    $roles_to_unset = array('editor','author','contributor','subscriber');

    foreach ($roles_to_unset as $key => $value) {

        unset($all_roles[$value]);

    }

    return $all_roles;

}

/**
 *  
 *  Checks if the current user has permissions to view the current
 *  assignment
 *
 * @author Victor Figueroa
 * @param  string $assignment Assignment Slug
 * @return wp_die() with message
 */
function check_current_user_assignment_permissions($assignment) {

    if(!empty($assignment)) {

        $assignment = returnPageTitleStr($assignment);

        $jcah_rs_roles = query_user_rs_assinged_group_name();

        if(!in_array($assignment, $jcah_rs_roles) && 
           !in_array('[WP jcah_editor]', $jcah_rs_roles) &&
           !in_array('[WP jcah_admin]', $jcah_rs_roles) &&
           !in_array('[WP administrator]', $jcah_rs_roles)) {

            return wp_die(__('Your account does not have permission to access this assignment (' . $assignment . ').'));
        }

    }

}


/**
 *  
 *  Give user a list of actions he/she can take according to the
 *  capabilities assigned.
 *
 * @author Victor Figueroa
 * @param  array global $all_roles
 * @return array global $all_roles
 */
function actions_allowed_for_user() {
    
    // first check if user is logged in
    if( is_user_logged_in() ) {

        // link to user profile
        echo '<li><a href="' . get_option('siteurl') . '/wp-admin/profile.php"><i class="icon-user icon-white"></i> Profile</a></li>';
        
        if( current_user_can( 'edit_documents' ) ) {

            echo '<li class="divider"></li>';
            
            // Link to manage post
            echo '<li><a href="/wp-admin/edit.php"><i class="icon-pencil icon-white"></i> Manage Posts</a></li>';
            
            // Link to manage documents
            echo '<li><a href="/wp-admin/edit.php?post_type=document"><i class="icon-file icon-white"></i> Manage Documents</a></li>';
            
            // Link to manage frequently asked questions
            echo '<li><a href="/wp-admin/edit.php?post_type=jcah_faq"><i class="icon-question-sign icon-white"></i> Manage FAQs</a></li>';
        
        }

        if( current_user_can( 'list_users' ) ) {

            echo '<li class="divider"></li>';
            
            // Link to list users
            echo '<li><a href="/wp-admin/users.php"><i class="icon-th-list icon-white"></i> List Users</a></li>';
            
            if( current_user_can( 'add_users' ) ) {
            
                // Link to manage users
                echo '<li><a href="/wp-admin/user-new.php"><i class="icon-plus-sign icon-white"></i> Add User</a></li>';

            }
        
        }

        if( current_user_can( 'delete_plugins' ) ) {

            echo '<li class="divider"></li>';
            
            // Link to manage plugins
            echo '<li><a href="/wp-admin/plugins.php"><i class="icon-cog icon-white"></i> Manage Plugins</a></li>';
            
            // Link to manage restrictions
            echo '<li><a href="/wp-admin/admin.php?page=rs-category-restrictions_t"><i class="icon-lock icon-white"></i> Manage Restrictions</a></li>';

            // Link to manage restrictions
            echo '<li><a href="/wp-admin/users.php?page=roles"><i class="icon-lock icon-white"></i> Manages Roles</a></li>';
        
        }

        echo '<li class="divider"></li>';
            
        // Logout link
        echo '<li><a href="' . wp_logout_url( home_url() ) . '"><i class="icon-off icon-white"></i> Logout</a></li>';    
    }

}