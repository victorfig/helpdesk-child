<?php
/**
 * Get all assigned document categories
 *
 * @author Victor Figueroa
 * @param  none
 * @return none
 */
// function getAssignmentsCatObjList()
// {
// 	// Setup Category Object
// 	$assignments = get_categories( array(
// 		'type' => 'document',
// 		'orderby' => 'slug',
// 		'order' => 'ASC',
// 		'hide_empty' => 1,
// 		'parent' => '0',
// 		'taxonomy' => 'category',
// 		'exclude' => '1'
// 		) 
// 	);

// 	return $assignments;
// }

// function createMainNavAssignmentList()
// {
// 	$catBase = get_option( 'category_base' );	// Category base set by user	
// 	$assignments = getAssignmentsCatObjList();	// Get Assignments Category Object

// 	if(!empty($assignments)) {
// 		echo '<div class="nav-collapse">';
// 		echo '<ul id="menu-main-menu" class="nav">';
// 		echo '<li id="menu-item-5" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children dropdown "><a href="#" class="dropdown-toggle" data-toggle="dropdown">Assignments<b class="caret"></b></a>';
// 		echo '<ul class="dropdown-menu">';
		
// 		foreach($assignments as $assignment)
// 			echo '<li id="menu-item-37" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children dropdown "><a href="' . get_bloginfo(url) . '/' . $catBase . '/' . $assignment->slug . '">' . $assignment->name . '</a></li>';
		
// 		echo '</ul>';
// 		echo '</li>';
// 		echo '</ul>';							
// 		echo '</div>';
// 	}
// }


/**
 * Return an array of id from an array of category slug names
 *
 * @author Victor Figueroa
 * @param  array $cat_array Array of categories
 * @return array $categories IDs of all categories provided
 */
function return_ids_of_category_array($cat_array) {
	
	foreach($cat_array as $cat) {
	
		$categories[] = get_cat_ID($cat);
	
	}
	
	return $categories;
}



