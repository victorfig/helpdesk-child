<?php
add_action("publish_post", "generate_jcah_sitemap");
add_action("publish_page", "generate_jcah_sitemap");
/**
 * Generate a sitemap that include all posts from all cpts added to
 * 'post_type' element and pages
 *
 * @author Victor Figueroa
 * @param  none
 * @return Update sitemap on root folder
 */
function generate_jcah_sitemap() {
  
  $postsForSitemap = get_posts(array(
  
    'numberposts' => -1,
  
    'orderby' => 'modified',
  
    'post_type'  => array('post','page','document','jcah_faq'),
  
    'order'    => 'DESC'
  
  ));

  $sitemap = "<?xml version='1.0' encoding='UTF-8'?>";
  
  $sitemap .= "<urlset xmlns='http://www.sitemaps.org/schemas/sitemap/0.9'>";

  foreach($postsForSitemap as $post) {
  
    setup_postdata($post);

    $postdate = explode(" ", $post->post_modified);

    $urlStr = get_permalink($post->ID);

    // Clean out permalink
    $urlStr = htmlspecialchars($urlStr);

    $sitemap .= "<url>".
     
      "<loc>". $urlStr ."</loc>".
     
      "<lastmod>". $postdate[0] ."</lastmod>".
     
      "<changefreq>monthly</changefreq>".
    
    "</url>";
  
  }

  $sitemap .= "</urlset>";

  $fp = fopen(ABSPATH . "sitemap.xml", 'w');
  
  fwrite($fp, $sitemap);
  
  fclose($fp);

}