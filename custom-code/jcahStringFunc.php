<?php
/**
 * Sanitize strings
 *
 * @author Victor Figueroa
 * @param  string $str Tag name or slug
 * @return string $str Tag without hiphen and first letter of each word capital
 */
function strSantizeTagStrings($str){
	
	if(isset($str)) {
	
		$str = strReplaceHiphensWithSpace($str);
	
		$str = strMakeFirstLetterOfEachWordsCapital($str);
	
	}
	
	return $str;
}

/**
 * Remove hiphens in string
 *
 * @author Victor Figueroa
 * @param  string $str Any string
 * @return string $str Without hiphen(s)
 */
function strReplaceHiphensWithSpace($str){
	
	if(isset($str)) {
	
		return $str = str_replace("-"," ", $str);
	}

}

/**
 * Uppercase first letter of each word
 *
 * @author Victor Figueroa
 * @param  string $str Any string
 * @return string $str first letter of each word uppercased
 */
function strMakeFirstLetterOfEachWordsCapital($str){
	
	if(isset($str)) {
	
		return $str = ucwords($str);
	
	}

}

/**
 * Uppercase all letters
 *
 * @author Victor Figueroa
 * @param  string $str Any string
 * @return string $str All letters of each are uppercased
 */
function strMakeAllLettersCapital($str){
	
	if(isset($str)) {
	
		return $str = strtoupper($str);
	
	}

}

/**
 * Configure breadcrumb by replacing hiphen with space and uppercasing first letter
 *
 * @author Victor Figueroa
 * @param  string $str Category slug or page title slug
 * @return string $str Remove all hiphens uppercase first letter of each word
 */
function strSantizeTagBreanCrumb($str) {
	
	if($str == 'jc-it' || $str == 'jc-avit' || $str == 'faq') {
	
		$str = strMakeAllLettersCapital($str);		
	
	} else {
	
		$str = strReplaceHiphensWithSpace($str);
	
		$str = strMakeAllLettersCapital($str);
	
	}
	
	return $str;

}


/**
 * Configure page titles by replacing hiphen with space and uppercasing first letter
 *
 * @author Victor Figueroa
 * @param  string $str Page title name
 * @return string $str Remove hiphens and uppercase first letter of each word
 */
function strSantizePageTitle($str) {
	
	if($str == 'JC-IT' || $str == 'JC-AVIT' || $str == 'faq') {
	
		$str = strMakeAllLettersCapital($str);		
	
	} else {
	
		$str = strReplaceHiphensWithSpace($str);
	
		$str = strMakeFirstLetterOfEachWordsCapital($str);
	
	}
	
	return $str;

}

/**
 * Configure template page titles by replacing hiphen with space and uppercasing first letter
 *
 * @author Victor Figueroa
 * @param  string $str Template Page title slug
 * @return string $str Remove hiphens and uppercase first letter of each word
 */
function strSantizeTagTemplatePageTitle($str) {
	
	if($str == 'jc-it' || $str == 'jc-avit' || $str == 'faq') {
	
		$str = strMakeAllLettersCapital($str);		
	
	} else {
	
		$str = strReplaceHiphensWithSpace($str);
	
		$str = strMakeFirstLetterOfEachWordsCapital($str);
	
	}
	
	return $str;

}

/**
 * Return page titles in a similar format as categories
 *
 * @author Victor Figueroa
 * @param  string $str Template Page title slug
 * @return string $str Remove hiphens and uppercase first letter of each word
 */
function returnPageTitleStr($str) {
	
	if($str == 'jc-it' || $str == 'jc-avit' || $str == 'faq') {
	
		$str = strMakeAllLettersCapital($str);	
	
	} else {
	
		$str = strReplaceHiphensWithSpace($str);
	
		$str = strMakeFirstLetterOfEachWordsCapital($str);
	
	}
	
	return $str;

}

/**
 * Display page title consistenly 
 *
 * @author Victor Figueroa
 * @param  string $str Template Page title slug
 * @return string Echo page title appropriately
 */
function printSanitizedPageTitle($str) {
	
	$str = strSantizeTagTemplatePageTitle($str);

	if($_GET['doc_tag'] == 'faq') {
		
		echo '<h2>' . $str . ' FAQ</h2>';
		echo '<hr class="page-title-hr"/>';
	
	}else{
	
		echo '<h2>' . $str . '</h2>';
		echo '<hr class="page-title-hr"/>';
	}
}