<?php
/**
 * Removes public faq posts that use the same tags as News items from any tag archive page
 *
 * @author Victor Figueroa
 * @param  array $query WordPress global array
 * @return array $query Altered WordPress global array
 */
#add_action( 'pre_get_posts', 'exclude_faq_from_tag_template' ); // Temporarily commented out

function exclude_faq_from_tag_template( $query ) {

    if (is_tag() && !is_home()) {

        $faqId = (array)returnSingleCatIdFromSlugString('faq',false);

        $query->set( 'category__not_in', $faqId );
    }

}