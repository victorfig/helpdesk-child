<?php
add_filter( 'wp_mail_content_type', 'jcah_change_email_content_type' );
/**
 * Change the content type of emails to HTML
 *
 * @author Victor Figueroa
 * @param  none
 * @return text/html
 */
function jcah_change_email_content_type( $content_type ) {
	
	return 'text/html';

}

add_filter( 'wp_mail', 'jcah_change_email_headers' );
/**
 * Change header content type of emails to HTML
 *
 * @author Victor Figueroa
 * @param  none
 * @return text/html
 */
function jcah_change_email_headers( $params ) {

	$params['headers'] = 'Content-type: text/html';
	
	return $params;

}

