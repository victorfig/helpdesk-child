<?php
/*
 * ONLY EDIT USING CUSTOM FIELDS PLUGIN THEN EXPORT
 * TO PHP AND REPLACE THE CODE BELOW
 */
if(function_exists("register_field_group"))
{
	// register_field_group(array (
	// 	'id' => 'acf_faq-related-documents',
	// 	'title' => 'FAQ Related Documents',
	// 	'fields' => array (
	// 		array (
	// 			'key' => 'field_52c3870dc460b',
	// 			'label' => 'Related Documents (optional)',
	// 			'name' => 'related_documents',
	// 			'type' => 'relationship',
	// 			'instructions' => 'Select one or more documents you would like to associate to this frequently asked question.',
	// 			'return_format' => 'object',
	// 			'post_type' => array (
	// 				0 => 'document',
	// 			),
	// 			'taxonomy' => array (
	// 				0 => 'all',
	// 			),
	// 			'filters' => array (
	// 				0 => 'search',
	// 			),
	// 			'result_elements' => array (
	// 				0 => 'post_title',
	// 			),
	// 			'max' => '',
	// 		),
	// 	),
	// 	'location' => array (
	// 		array (
	// 			array (
	// 				'param' => 'post_type',
	// 				'operator' => '==',
	// 				'value' => 'jcah_faq',
	// 				'order_no' => 0,
	// 				'group_no' => 0,
	// 			),
	// 		),
	// 	),
	// 	'options' => array (
	// 		'position' => 'normal',
	// 		'layout' => 'default',
	// 		'hide_on_screen' => array (
	// 		),
	// 	),
	// 	'menu_order' => 0,
	// ));
	
	register_field_group(array (
		'id' => 'acf_document-description',
		'title' => 'Document Description',
		'fields' => array (
			array (
				'key' => 'field_52a92ff38c800',
				'label' => 'Description',
				'name' => 'description',
				'type' => 'text',
				'instructions' => 'Describe the contents of this documents with 144 characters or less.',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => 144,
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'document',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
				0 => 'comments',
				1 => 'slug',
				2 => 'author',
				3 => 'format',
				4 => 'featured_image',
				5 => 'send-trackbacks',
			),
		),
		'menu_order' => 1,
	));
}
