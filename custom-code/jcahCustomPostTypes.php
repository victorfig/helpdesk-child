<?php
add_action( 'init', 'register_cpt_jcah_faq' );
/**
 * Register FAQ Custom Post Type
 *
 * @author Victor Figueroa
 * @param  none
 * @return none
 */
function register_cpt_jcah_faq() {

    $labels = array( 
        'name' => _x( 'FAQs', 'jcah_faq' ),
        'singular_name' => _x( 'FAQ', 'jcah_faq' ),
        'add_new' => _x( 'Add New', 'jcah_faq' ),
        'add_new_item' => _x( 'Add New FAQ', 'jcah_faq' ),
        'edit_item' => _x( 'Edit FAQ', 'jcah_faq' ),
        'new_item' => _x( 'New FAQ', 'jcah_faq' ),
        'view_item' => _x( 'View FAQ', 'jcah_faq' ),
        'search_items' => _x( 'Search FAQs', 'jcah_faq' ),
        'not_found' => _x( 'No FAQs found', 'jcah_faq' ),
        'not_found_in_trash' => _x( 'No FAQs found in Trash', 'jcah_faq' ),
        'parent_item_colon' => _x( 'Parent FAQ:', 'jcah_faq' ),
        'menu_name' => _x( 'FAQs', 'jcah_faq' ),
    );

    $args = array( 
        'labels' => $labels,
        'hierarchical' => false,
        'description' => 'Frequenly asked questions',
        'supports' => array( 'title', 'editor' ),
        'taxonomies' => array( 'assignments', 'doc_tag' ),
        'show_in_menu' => true,
        'public' => true,
        'show_ui' => true,
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'capability_type' => 'jcah_faq',
        'capabilities' => array(
            'publish_posts' => 'publish_faqs',
            'edit_posts' => 'edit_faqs',
            'edit_others_posts' => 'edit_others_faqs',
            'edit_published_posts' => 'edit_published_faqs',
            'delete_posts' => 'delete_faqs',
            'delete_others_posts' => 'delete_others_faqs',
            'delete_published_posts' => 'delete_published_faqs',
            'read_private_posts' => 'read_private_faqs',
            'edit_post' => 'edit_faq',
            'delete_post' => 'delete_faq',
            'read_post' => 'read_faq',
            'create_posts' => 'edit_faqs',
            'read' => 'read'
        ),
        'map_meta_cap' => true
    );

    register_post_type( 'jcah_faq', $args );
}