<?php
/**
 * Format document date
 *
 * @author Victor Figueroa
 * @param  string $date String of date, including time
 * @return string $date String of formatted date without time
 */
function formatModifiedDate($date) {
	
    $boolean = preg_match('.(\d{4})-(\d{2})-(\d{2}).', $date, $matches);
	
    if($boolean == true) {
	
    	$modifiedDate = new DateTime($matches[0]);
	
    	$date = $modifiedDate->format("F j, Y");
	
    }
	
    return $date;
}

/**
 * Accept a multi-demensional category array of standard class Object
 *
 * @author Victor Figueroa
 * @param  array $multi_arrays Multidimensional array
 * @return array $tag_array Associative array with IDs of all categories as key and name as value
 */
function createSimpleArrayFromMultiDimensionalArray($multi_arrays) {
	
    if(!empty($multi_arrays)) {
    
        foreach($multi_arrays as $multi_array) {
    
            if(!empty($multi_array)) {
    
        		foreach($multi_array as $array) {
    
        			$tag_array[$array->term_id] = (array) $array;
    
        		}
    
            }
    
    	}
    
        return $tag_array;

    }

}

/**
 * For SEO purposes, use to display tags associated with a document on front-end
 *
 * @author Victor Figueroa
 * @param  int $post_id Post ID
 * @param  string $before What to display before
 * @param  string $taxonomy Taxonomy slug name
 * @param  string $seperator What to use for separating tags
 * @return string $html Tags to be included in an element attribute
 */
function commaSeparatedTagList($post_id, $before, $taxonomy, $seperator) {
	
    $posttags = get_the_terms($post_id, $taxonomy);

	$html = '';
	
    foreach ( $posttags as $posttag ) {
    
        if($posttag->slug != 'faq'){
    
            $html .= $before . "{$posttag->slug}" . $seperator;
    
        }
    
    }
	
    $html .= '';
	
	return trim($html);
}

function get_tag_ID($tag_name) {
$tag = get_term_by('name', $tag_name, 'post_tag');
if ($tag) {
return $tag->term_id;
} else {
return 0;
}
}


add_action( 'init', 'register_custom_jcah_taxonomies' );
/**
 * Register Taxonomies
 *
 * @author Victor Figueroa
 * @param  none
 * @return WordPress taxonomies
 */
function register_custom_jcah_taxonomies() {
	
	/* Register hierarchical taxonomies
	 ****************************************** */

	// Taxonomy: Assignments -> PLEASE DO NOT CHANGE
	$one_custom_taxonomy_name 	= "assignments";
	
	// Labels and slug names
	$one_singular_label_name 	= "Assignment";
	$one_singular_slug_name 	= "assignment";
	$one_plural_label_name 		= "Assignments";
	$one_plural_slug_name 		= "assignments";

    $one_assignment_labels = array( 
        'name' 							=> _x( $one_plural_label_name, $one_custom_taxonomy_name ),
        'singular_name' 				=> _x( $one_singular_label_name, $one_custom_taxonomy_name ),
        'search_items' 					=> _x( 'Search ' . $one_plural_label_name, $one_custom_taxonomy_name ),
        'popular_items' 				=> _x( 'Popular ' . $one_plural_label_name, $one_custom_taxonomy_name ),
        'all_items' 					=> _x( 'All ' . $one_plural_label_name, $one_custom_taxonomy_name ),
        'parent_item' 					=> _x( 'Parent ' . $one_singular_label_name, $one_custom_taxonomy_name ),
        'parent_item_colon' 			=> _x( 'Parent ' . $one_singular_label_name . ':', $one_custom_taxonomy_name ),
        'edit_item' 					=> _x( 'Edit ' . $one_singular_label_name, $one_custom_taxonomy_name ),
        'update_item' 					=> _x( 'Update ' . $one_singular_label_name, $one_custom_taxonomy_name ),
        'add_new_item' 					=> _x( 'Add New ' . $one_singular_label_name, $one_custom_taxonomy_name ),
        'new_item_name' 				=> _x( 'New ' . $one_singular_label_name, $one_custom_taxonomy_name ),
        'separate_items_with_commas' 	=> _x( 'Separate ' . $one_plural_slug_name . ' with commas', $one_custom_taxonomy_name ),
        'add_or_remove_items' 			=> _x( 'Add or remove ' . $one_plural_slug_name, $one_custom_taxonomy_name ),
        'choose_from_most_used' 		=> _x( 'Choose from the most used ' . $one_plural_slug_name, $one_custom_taxonomy_name ),
        'menu_name' 					=> _x( $one_plural_label_name, $one_custom_taxonomy_name ),
    );

    $one_assignment_args = array(
        'labels' 			=> $one_assignment_labels,
        'public' 			=> true,
        'show_in_nav_menus' => true,
        'show_ui' 			=> true,
        'show_tagcloud' 	=> false,
        'show_admin_column' => false,
        'hierarchical' 		=> true,

        'rewrite' 			=> array('slug' => 'assignments'),
        'query_var' 		=> true
    );

    register_taxonomy( $one_custom_taxonomy_name, array('document'), $one_assignment_args );


	
	/* Register nonhierarchical taxonomies
	 ****************************************** */

	// Taxonomy -> Doc Tag
	$two_custom_taxonomy_name 	= "doc_tag";
	
	// Labels and slug names
	$two_singular_label_name 	= "Document Tag";
	$two_singular_slug_name 	= "document tag";
	$two_plural_label_name 		= "Tags";
	$two_plural_slug_name 		= "document tags";

    $two_assignment_labels = array( 
        'name' 							=> _x( $two_plural_label_name, $two_custom_taxonomy_name ),
        'singular_name' 				=> _x( $two_singular_label_name, $two_custom_taxonomy_name ),
        'search_items' 					=> _x( 'Search ' . $two_plural_label_name, $two_custom_taxonomy_name ),
        'popular_items' 				=> _x( 'Popular ' . $two_plural_label_name, $two_custom_taxonomy_name ),
        'all_items' 					=> _x( 'All ' . $two_plural_label_name, $two_custom_taxonomy_name ),
        'parent_item' 					=> null,
        'parent_item_colon' 			=> null,
        'edit_item' 					=> _x( 'Edit ' . $two_singular_label_name, $two_custom_taxonomy_name ),
        'update_item' 					=> _x( 'Update ' . $two_singular_label_name, $two_custom_taxonomy_name ),
        'add_new_item' 					=> _x( 'Add New ' . $two_singular_label_name, $two_custom_taxonomy_name ),
        'new_item_name' 				=> _x( 'New ' . $two_singular_label_name, $two_custom_taxonomy_name ),
        'separate_items_with_commas' 	=> _x( 'Separate ' . $two_plural_slug_name . ' with commas', $two_custom_taxonomy_name ),
        'add_or_remove_items' 			=> _x( 'Add or remove ' . $two_plural_slug_name, $two_custom_taxonomy_name ),
        'choose_from_most_used' 		=> _x( 'Choose from the most used ' . $two_plural_slug_name, $two_custom_taxonomy_name ),
        'menu_name' 					=> _x( $two_plural_label_name, $two_custom_taxonomy_name ),
    );

    $two_assignment_args = array(
        'labels' 			=> $two_assignment_labels,
        'public' 			=> true,
        'show_in_nav_menus' => true,
        'show_ui' 			=> true,
        'show_tagcloud' 	=> false,
        'show_admin_column' => false,
        'hierarchical' 		=> false,

        'rewrite' 			=> array('slug' => 'doctag'),
        'query_var' 		=> true
    );

    register_taxonomy( $two_custom_taxonomy_name, array('document'), $two_assignment_args );

} // End of unregister_register_taxonomies

/**
 * Present post revisions only to authorize users
 *
 * @author Victor Figueroa
 * @param  int $doc_id Document ID
 * @return List of previous revisions to document in a collapsible format
 */
function list_jcah_document_revisions($doc_id) {
    
    // Get current user information
    global $current_user;
    get_currentuserinfo();

    if($current_user->user_login == 'mdellaquila' || $current_user->user_login == 'admin') {
    
        $doc_past_revisions = get_document_revisions($doc_id); ?>
    
        <br />
    
        <button type="button" class="btn btn-mini" data-toggle="collapse" data-target="#collapse<?php echo $doc_id;?>" title="Past revisions for <?php echo $doc_past_revisions[0]->post_title; ?>">
    
        Past Reveisions
    
        </button>

        <div class="accordion" id="accordion2">
    
            <div id="collapse<?php echo $doc_id; ?>" class="accordion-body collapse">
    
                <div class="accordion-inner" style="border-top:none;!important">
    
                <small>
    
                    <table class="table table-striped table-condensed">
    
                        <thead>
    
                            <th width="20"></th>
    
                            <th width="130">Modified</th>
    
                            <th width="50">Author</th>
    
                            <th>Summary</th>
    
                        </thead>
    
                        <tbody>
    
                            <?php foreach($doc_past_revisions as $doc_past_revision): ?>
    
                                <?php $postauthor = get_user_by('id',$doc_past_revision->post_author); ?>
    
                                <?php if($doc_id != $doc_past_revision->ID): ?>
    
                                    <tr title="<?php echo $doc_past_revision->post_title; ?> was modified by <?php echo $postauthor->user_nicename; ?> on <?php echo $revisionDate = formatModifiedDate($doc_past_revision->post_modified); ?>">
    
                                        <td><a href="<?php echo $doc_past_revision->guid; ?>" title="Download <?php echo $doc_past_revision->post_title; ?>"><i class="icon-download-alt"></i></a></td>
    
                                        <td><?php echo $revisionDate = formatModifiedDate($doc_past_revision->post_modified); ?></td>
    
                                        <td><?php echo $postauthor->user_nicename; ?></td>
    
                                        <td><?php echo $doc_past_revision->post_excerpt; ?></td>
    
                                    </tr>
    
                                <?php endif;?>
    
                            <?php endforeach; ?>
    
                        </tbody>
    
                    </table>
    
                    </small>
    
                </div>
    
            </div>
    
        </div><?php
    
    } else {
    
        echo "<div style='height:20px;'></div>";
    
    }

}