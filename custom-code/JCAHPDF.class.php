<?php
// CONSTANTS
define('FPDF17_CLASSPATH', get_template_directory() . '-2-child/3rdparty-classes/fpdf17');
define('FPDF_FONTPATH', get_template_directory() . '-2-child/3rdparty-classes/fpdf17/font');

// Include FPDF Class
require(FPDF17_CLASSPATH . '/fpdf.php');

class JCAHPDF extends FPDF {

	var $assignmentName;
	var $documentHeader = 'JCAH Helpdesk';

	/**
	 *
	 *	Getter for assignment name to display underneath page title
	 *
	 */

	function AssignmentName($str) {

		return $this->assignmentName = $str;

	}

	function Header() {

		// Add font
		$this->AddFont('Helvetica','','helvetica.php');

		// Set font
		$this->SetFont('Helvetica');
	
	    // Select Arial bold 15
	    $this->SetFont('Helvetica','B',20);
		
		// Set fill color
	    $this->SetTextColor(44, 62, 80);

	    // Framed title
	    $this->Cell(30,10,$this->documentHeader);

	    $this->SetTextColor(44, 62, 80);

	    $this->SetFont('Helvetica','',13);

	    $this->Ln();

	    $this->Cell(30,10,$this->assignmentName);
	
	    // Line break
	    $this->Ln(12);
	
	}

	function Footer() {
	    
	    //Position at 1.5 cm from bottom
	    $this->SetY(-15);
	    
	    //Arial italic 8
	    $this->SetFont('Helvetica','I',8);
	    
	    //Page number
	    $this->Cell(0,10,'Page '.$this->PageNo(),0,0,'C');
	
	}

}