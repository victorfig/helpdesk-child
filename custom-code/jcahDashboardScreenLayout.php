<?php
#add_filter('screen_layout_columns', 'wpjcah_layout_columns');
/**
 * Format layout of metaboxes in edit post and document screens
 *
 * @author Victor Figueroa
 * @param  array $columns Global array of post types
 * @return array $columns Altered columns array
 */
function wpjcah_layout_columns ($columns) {
	
	$columns['post'] = 1; // Force post Post-Type to only offer 1 column
	
	$columns['document'] = 1; // Force document Post-Type to only offer 1 column
	
	return $columns;
}


#add_filter('get_user_option_screen_layout_post', 'so_screen_layout_post');
/**
 * Only provide one column in dashboard screen layout setting
 *
 * @author Victor Figueroa
 * @param  none
 * @return int Number of columns
 */
function so_screen_layout_post() {
	
	return 1;

}

#add_filter('get_user_option_screen_layout_document', 'so_screen_layout_document');
/**
 * Force document Post-Type to 1 column
 *
 * @author Victor Figueroa
 * @param  none
 * @return int Number of columns for document post type
 */
function so_screen_layout_document() {
	
	return 1;

}

add_action( 'admin_menu', 'my_remove_menu_pages' );
/**
 * Removes specific menu page from dashboard if user does not have
 * the capability of installing new themes
 *
 * @author Victor Figueroa
 * @param  none
 * @return string $logouturl Amended with redirect url
 */
function my_remove_menu_pages() {

    if( (is_user_logged_in() && !current_user_can('install_themes')) ) {
    
        remove_menu_page('index.php');
        
        remove_menu_page('edit-comments.php');
        
        remove_menu_page('tools.php');

    }

}