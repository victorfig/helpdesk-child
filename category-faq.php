<?php get_header(); $currentPage = 'category_faq';?>

<?php global $newsCatObj; $newsCatObj = $wp_query->queried_object;?>

<div id="content" class="clearfix row-fluid">

	<div id="main" class="span8 clearfix" role="main">

		<!-- Breadcrumb -->
		<small style="font-size:12px;">
			
			<ul class="breadcrumb">
		    
		        <li><a href="/">HOME</a> <span class="divider">/</span></li>
		    
		        <li class="active"><?php echo $assignment_name = strMakeAllLettersCapital($newsCatObj->name);?></li>
	    	
	    	</ul>
	    
	    </small>

	    <?php printSanitizedPageTitle($newsCatObj->slug); ?>

	    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				<?php $tags[] = get_the_terms($post->ID, 'post_tag'); ?>
		
		<?php endwhile; else: ?> <!-- end loop -->
		
			<span class="label label-inverse"><?php _e('No tags available'); ?></span>
		
		<?php endif; ?>

		<?php rewind_posts(); ?>

		<?php $postTags = createSimpleArrayFromMultiDimensionalArray($tags); #print_r($postTags);?>

		<?php if(!empty($postTags)): ?>

			<a href="#_" class="expandcollapse btn btn-mini btn-primary pull-right" state="0"><i class='icon-white icon-minus-sign'></i> Collapse All</a> <?php global $collapseBtn; $collapseBtn = true; ?>

			<?php foreach($postTags as $postTag) { $thisPostTags[]=$postTag['slug']; }?>

			<?php foreach($thisPostTags as $thisPostTag):?>

				<?php if($thisPostTag != 'faq'):?>
						
						<a name="<?php echo $thisPostTag; ?>"></a>
						
						<div class="accordion">
						    <!-- Sections -->

						    <div class="accordion-group faq-remove-borders">
						        <!-- Section -->
						        
						        <div class="accordion-heading">
						        
						            <h4>
						            
						            	<a href="#top"><i class="icon-arrow-up faq-up-arrow pull-left" style="margin-top:10px;"></i></a>

						            	<a class="accordion-toggle faq-section" data-toggle="collapse" href="#collapse-<?php echo $thisPostTag;?>">
						
											<?php echo strSantizeTagStrings($thisPostTag); ?>
						
										</a>

						            </h4>
						        
						        </div><!-- /Section -->
						
								<ul>
									<div class="accordion-body collapse in" id="collapse-<?php echo $thisPostTag;?>">
							            
							            <div class="accordion-inner faq-remove-borders">
							                
							                <div class="accordion" id="<?php echo $thisPostTag;?>">
							                    <!-- SubSections -->
							                
											<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

												<!-- To enable video uncomment the following code and erase this comment -->
												<?php #$strToSearch = get_the_content(); $strJwplayer = 'jwplayer'; $strVideo = 'video'; ?>

												<?php if(is_object_in_term($post->ID, 'post_tag', $thisPostTag)):?>

													<div class="accordion-group faq-remove-borders">
									                    
									                        <div class="accordion-heading question">
									                    
									                            <a class="accordion-toggle" data-parent=
									                            "#<?php echo $thisPostTag;?>-<?php the_ID(); ?>" data-toggle="collapse" href=
									                            "#showQuestion<?php echo $thisPostTag;?>-<?php the_ID(); ?>" style="text-decoration:none;"><?php the_title(); ?></a>
									                    
									                        </div><!-- Questions -->

																<div class="accordion-body collapse in" id="showQuestion<?php echo $thisPostTag;?>-<?php the_ID(); ?>">
									                    
									                            <div class="accordion-inner faq-remove-borders">
									                    
									                                <div class="accordion" id="showQuestion<?php echo $thisPostTag;?>-<?php the_ID(); ?>">
									                    
									                                    <div class="accordion-group faq-remove-borders">
									                                        
									                                        <div style="margin-top:-12px;">
															
																			<?php if($post->post_type == 'post'): ?>
															
																				<small class="muted">FAQ updated <?php echo $date = formatModifiedDate($post->post_modified); ?></small>
															
																				<?php the_content(); ?>
															
																			<?php endif;?>
															
																		</p>
															
																	</div>

									                                    </div>
									             
									                                </div>
									             
									                            </div>
									             
									                        </div><!-- /Questions -->
									             
									                    </div><!-- /SubSection -->
										
												<?php endif;?> <!-- End of Term Check -->
										
											<?php endwhile; else: ?> <!-- end while loop -->
										
												<span class="label label-inverse"><?php _e('No FAQs associated with this tag'); ?></span>
										
											<?php endif; // end loop if statement?>
										
										</div><!-- /SubSections -->

							            </div>

							        </div>
								
								</ul>
							
							</div> <!-- End accordion-group div -->
						
						</div> <!-- End accordion div -->

				<?php endif;?> <!-- End if thisPostTag != faq -->

			<?php endforeach; ?> <!-- End foreach thisPostTag -->

		<?php endif; ?>
		
		<?php wp_reset_postdata(); //Restore original Post Data ?>
		
	</div> <!-- end #main -->

	<?php get_sidebar(); // sidebar 1 ?>

</div> <!-- end #content -->

<?php if(isset($collapseBtn)):?>
	<script type="text/javascript">
		jQuery(document).ready(function($) {

			//collapsible menu
		    $('.expandcollapse').click(function() {

		        var newstate = $(this).attr('state') ^ 1,
		            icon = newstate ? "plus" : "minus",
		            text = newstate ? "Collapse" : "Expand";
		    
		        // if state=0, show all the accordion divs within the same block (in this case, within the same section)
		        if ( $(this).attr('state')==="0" ) {
		            console.log('2');
		            $(this).siblings('div').find('div.accordion-body.in').collapse('hide');
		        }
		        // otherwise, collapse all the divs
		        else {
		            console.log('1');
		            $(this).siblings('div').find('div.accordion-body:not(.in)').collapse('show');
		        }

		        $(this).html("<i class=\"icon-white icon-" + icon + "-sign\"></i> " + text +" All");

		        $(this).attr('state',newstate)

		    });

		    $('a[data-toggle="tab"]').on('shown', function (e) {

		        var myState = $(this).attr('state'),
		            state = $('.expandcollapse').attr('state');

		        if(myState != state) {
		          toggleTab($(this).prop('hash'));
		          $(this).attr('state',state);
		        }

		    })

		    function toggleTab(id){

		        $(id).find('.collapse').each(function() {
		            $(this).collapse('toggle');
		          });

		    }
	    });
	</script>
<?php endif; ?>

<?php get_footer(); ?>
